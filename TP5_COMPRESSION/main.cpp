#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <cmath>

#include "Vec3.h"

using namespace std;


void readOFF(char* path, vector<Vec3>& vertices, vector<Vec3>& triangles)
{
	std::ifstream in (path);
    if (!in) exit (EXIT_FAILURE);

    std::string offString;
    unsigned int sizeV, sizeT, tmp;
    in >> offString >> sizeV >> sizeT >> tmp;

    vertices.resize (sizeV);
    triangles.resize (sizeT);
    for (unsigned int i = 0; i < sizeV; i++)
        in >> vertices[i];
    int s;
    for (unsigned int i = 0; i < sizeT; i++) {
        in >> s;
        for (unsigned int j = 0; j < 3; j++)
            in >> triangles[i].mVals[j];
    }

    in.close ();
}

int qp;
Vec3 bbMax, bbMin;
float range;

void computeBB(vector<Vec3>& vertices)
{
	bbMax = Vec3(-1000,-1000,-1000);
	bbMin = Vec3(1000,1000,1000);

	for(int i = 0; i < vertices.size(); i++)
	{
		bbMin[0] = min(bbMin[0],vertices.at(i)[0]);
		bbMin[1] = min(bbMin[1],vertices.at(i)[1]);
		bbMin[2] = min(bbMin[2],vertices.at(i)[2]);

		bbMax[0] = max(bbMax[0],vertices.at(i)[0]);
		bbMax[1] = max(bbMax[0],vertices.at(i)[1]);
		bbMax[2] = max(bbMax[0],vertices.at(i)[2]);
	}

	range = 0;
	range = max(range,abs(bbMin[0]-bbMax[0]));
	range = max(range,abs(bbMin[1]-bbMax[1]));
	range = max(range,abs(bbMin[2]-bbMax[2]));
}

int quantifyValue(float v,int coordId)
{
	return (v-bbMin[coordId]) * pow(2,qp) / range;
}

void quantify(vector<Vec3>& verticesIn, vector<Vec3>& verticesOut)
{
	verticesOut.clear();
	verticesOut.resize(verticesIn.size());

	for(int i = 0; i < verticesIn.size(); i++)
	{
		Vec3 c = verticesIn.at(i);
		
		c = Vec3(quantifyValue(c[0],0),quantifyValue(c[1],1),quantifyValue(c[2],2));

		verticesOut[i] = c;
	}
}

float dequantifyValue(float v, int coordId)
{
	return v * range / pow(2,qp) + bbMin[coordId];
}

void dequantify(vector<Vec3>& verticesIn, vector<Vec3>& verticesOut)
{
	verticesOut.clear();
	verticesOut.resize(verticesIn.size());

	for(int i = 0; i < verticesIn.size(); i++)
	{
		Vec3 c = verticesIn.at(i);
		
		c = Vec3(dequantifyValue(c[0],0),dequantifyValue(c[1],1),dequantifyValue(c[2],2));

		verticesOut[i] = c;
	}
}

Vec3 RMSE(vector<Vec3>& verticesIn, vector<Vec3>& verticesOut)
{

	float x = 0; float y = 0; float z = 0;
	for(int i = 0; i < verticesIn.size(); i++)
	{
		x += verticesIn.at(i)[0] - verticesOut.at(i)[0];
		y += verticesIn.at(i)[1] - verticesOut.at(i)[1];
		z += verticesIn.at(i)[2] - verticesOut.at(i)[2];

		x *= x; y *= y; z *= z;
	}

	x /= (float)verticesIn.size(); y /= (float)verticesIn.size(); z /= (float)verticesIn.size();

	return Vec3(sqrt(x),sqrt(y),sqrt(z));
}

void displayVertices(vector<Vec3>& vertices, int count)
{
	for(int i = 0; i < count; i++)
	{
		cout << "X : " << vertices.at(i)[0] << " Y : "  << vertices.at(i)[1] << " Z : " << vertices.at(i)[2] << endl;
	}
}

void saveData(vector<float> values, int start,char* path)
{
	ofstream flux(path);
	for(int i = start; i < values.size();i++)	{	flux << i << " " << values.at(i) << endl;}//values.at(i)[0] << " " << values.at(i)[1] << " " << values.at(i)[2] << std::endl;}
}

void getAlphabet(vector<Vec3>& vertices, int coordId, vector<float>& alphabet, vector<float>& probabilities)
{
	for(int i = 0; i < vertices.size(); i++)
	{
		bool contained = false; int id = 0;
		for(; id < alphabet.size(); id++)
		{
			if(alphabet.at(id) == vertices.at(i)[coordId]) {contained = true; break;}
		}

		if(contained)
		{
			probabilities[id]++;
		}
		else
		{
			alphabet.push_back(vertices.at(i)[coordId]);
			probabilities.push_back(1);
		}		
	}
}

void rANS(vector<Vec3>& vertices, vector<Vec3>& out)
{
	vector<float> alphabetX,alphabetY,alphabetZ, probabilitiesX,probabilitiesY,probabilitiesZ;

	cout << "Processing X alphabet" << endl;
	getAlphabet(vertices,0,alphabetX,probabilitiesX);
	cout << "Processing Y alphabet" << endl;
	getAlphabet(vertices,1,alphabetY,probabilitiesY);
	cout << "Processing Z alphabet" << endl;
	getAlphabet(vertices,2,alphabetZ,probabilitiesZ);

	cout << "Alphabet X : ";
	for(int i = 0; i < alphabetX.size(); i++){cout << alphabetX.at(i) << " ";}
	cout << endl;
	cout << "Alphabet Y : ";
	for(int i = 0; i < alphabetY.size(); i++){cout << alphabetY.at(i) << " ";}
	cout << endl;
	cout << "Alphabet Z : ";
	for(int i = 0; i < alphabetZ.size(); i++){cout << alphabetZ.at(i) << " ";}
	cout << endl;
}

int main(int argc, char **argv)
{
	srand(std::time(nullptr));

	if (argc != 3)
	{
		printf("Usage: Mesh In ; Mesh out ");
		return 1;
	}
	char meshInPath[250], meshOutPath[250];
	sscanf (argv[1],"%s",meshInPath) ;
	sscanf (argv[2],"%s",meshOutPath) ;

	vector<Vec3> vertices, triangles,vertices_q,vertices_out,vertices_rans;

	cout << "Reading " << meshInPath << " file" << endl;
	readOFF(meshInPath,vertices,triangles);

	cout << "Computing BB" << endl;
	computeBB(vertices);
	cout << "\t max : " << bbMax[0] << " "  << bbMax[1] << " " << bbMax[2] << endl;
	cout << "\t min : " << bbMin[0] << " "  << bbMin[1] << " " << bbMin[2] << endl;
	cout << "\t range : " << range << endl;

	//vector<float> values; 
	//for(int i = 0; i < 30; i++)
	//{
		//qp = i; Vec3 rmse = Vec3(0,0,0); float v = 0;
		//if(i > 2) 
		//{
		//	cout  << "<" << i << ">" << endl;

			qp = 5;
			//cout << "Quantifying" << endl;
			quantify(vertices,vertices_q);

			//cout << "Dequantiying" << endl;
			dequantify(vertices_q,vertices_out);

			//displayVertices(vertices,1);
			//displayVertices(vertices_q,1);
			//displayVertices(vertices_out,1);
			//cout << endl;
			
			//rmse = RMSE(vertices,vertices_out);

			//v = ((rmse[0]+rmse[1]+rmse[2])/3.0f);
			//cout << "RMSE : " << v << " " <<  rmse[0] << " "  << rmse[1] << " " << rmse[2] << endl;
		//}

		//values.push_back(v);
	//}

	//saveData(values,3,"data.dat");

	rANS(vertices_q,vertices_rans);

	//cout << "RMSE : " <<  rmse[0] << " "  << rmse[1] << " " << rmse[2] << endl;

	cout << "END" << endl;

	return 0;
}

#include "../Library/ImageBase.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

using namespace std;

void seuillagePGM(ImageBase* imgIn, ImageBase* imgOut)
{
  cout<< " > Seuillage PGM" <<endl;
  cout<< "  > Seuil : ";
  int seuil;scanf("%i",&seuil);
  
        for(int x = 0; x < imgIn->getHeight(); ++x)
	{
		for(int y = 0; y < imgIn->getWidth(); y++)
		{
		  imgOut->set(x,y,0,imgIn->get(x,y,0) > seuil ? 255 : 0);		      
		}
	}
}

void morphoBin(ImageBase* imgIn, ImageBase* imgOut,bool erosion,bool seuil) //or dilatation
{
  cout << (erosion ? "Erosion binaire" : "Dilatation binaire") << endl;
  
  ImageBase* imgSeuil;
  if(seuil){
    imgSeuil = new ImageBase(imgIn->getWidth(), imgIn->getHeight(),imgIn->getColor());
    seuillagePGM(imgIn,imgSeuil);
  }
  else
    {
      imgSeuil = imgIn;
    }

  int voisinsX[4] = {1,0,-1,0};
  int voisinsY[4] = {0,1,0,-1};
  
   for(int x = 0; x < imgSeuil->getHeight(); x++)
   {
       	for(int y = 0; y < imgSeuil->getWidth(); y++)
       	{
	  bool hasVoisin = false;
	  for(int i = 0; i < 4; i++)
	    {
	      if(x + voisinsX[i] < 0 || x + voisinsX[i] >= imgSeuil->getWidth()){continue;}
	      if(y + voisinsY[i] < 0 || y + voisinsY[i] >= imgSeuil->getWidth()){continue;}
	       
	      if(erosion && imgSeuil->get(x + voisinsX[i] , y + voisinsY[i],0) > 0){hasVoisin = true; break;}
	      if(!erosion && imgSeuil->get(x + voisinsX[i] , y + voisinsY[i],0) < 255){hasVoisin = true; break;}
	    }

	  if(erosion)
	    imgOut->set(x,y,0,hasVoisin ? 255 : (imgSeuil->get(x,y,0)));
	  else
	    imgOut->set(x,y,0,hasVoisin ? 0 : (imgSeuil->get(x,y,0))); 
       	}
   }
}

void morphoPGM(ImageBase* imgIn, ImageBase* imgOut,bool erosion) //or dilatation
{
  cout << (erosion ? "Erosion pgm" : "Dilatation pgm") << endl;

  int voisinsX[4] = {1,0,-1,0};
  int voisinsY[4] = {0,1,0,-1};
  
   for(int x = 0; x < imgOut->getHeight(); x++)
   {
       	for(int y = 0; y < imgOut->getWidth(); y++)
       	{
	  int maxVal = imgIn->get(x,y,0);
	  for(int i = 0; i < 4; i++)
	    {
	      if(x + voisinsX[i] < 0 || x + voisinsX[i] >= imgOut->getWidth()){continue;}
	      if(y + voisinsY[i] < 0 || y + voisinsY[i] >= imgOut->getWidth()){continue;}
	       
	      if(erosion && imgIn->get(x + voisinsX[i] , y + voisinsY[i],0) > maxVal){maxVal =imgIn->get(x + voisinsX[i] , y + voisinsY[i],0); }
	      if(!erosion && imgIn->get(x + voisinsX[i] , y + voisinsY[i],0) < maxVal){maxVal =imgIn->get(x + voisinsX[i] , y + voisinsY[i],0); }
	    }
	  
	    imgOut->set(x,y,0,maxVal);
       	}
   }
}

void doubleMorphoBin(ImageBase* imgIn, ImageBase* imgOut, int ouverture) // or fermeture
{
  cout << (ouverture ? "Ouverture" : "Fermeture") << endl;

  ImageBase* imgTemp = new ImageBase(imgIn->getWidth(), imgIn->getHeight(),imgIn->getColor());

    if(ouverture)
      {
	morphoBin(imgIn,imgTemp,true,true);
	morphoBin(imgTemp,imgOut,false,false);
      }
    else
      {
	morphoBin(imgIn,imgTemp,false,true);
	morphoBin(imgTemp,imgOut,true,false);
      }
}

void cleanerBin(ImageBase* imgIn, ImageBase* imgOut)
{
  cout << "Cleaner" << endl;

  bool operations[12] = {false,false,false,true,true,true,true,true,true,false,false,false};

  ImageBase* previous;
  for(int i = 0; i < 12; i++)
    {
      ImageBase* imgTemp = new ImageBase(imgIn->getWidth(), imgIn->getHeight(),imgIn->getColor());

      morphoBin(i == 0 ? imgIn : previous,i==11 ? imgOut : imgTemp,operations[i],i == 0);
      previous = imgTemp;
    }
}

void cleanerPGM(ImageBase* imgIn, ImageBase* imgOut)
{
  cout << "Cleaner PGM" << endl;

  bool operations[12] = {false,false,false,true,true,true,true,true,true,false,false,false};

  ImageBase* previous;
  for(int i = 0; i < 12; i++)
    {
      ImageBase* imgTemp = new ImageBase(imgIn->getWidth(), imgIn->getHeight(),imgIn->getColor());

      morphoPGM(i == 0 ? imgIn : previous,i==11 ? imgOut : imgTemp,operations[i]);
      previous = imgTemp;
    }
}

void cleanerPPM(ImageBase* imgIn, ImageBase* imgOut)
{
  cout << "Cleaner PPM" << endl;
  
  ImageBase* imgR = imgIn->getPlan(ImageBase::PLAN_R);
  ImageBase* imgG = imgIn->getPlan(ImageBase::PLAN_G);
  ImageBase* imgB = imgIn->getPlan(ImageBase::PLAN_B);

  cleanerPGM(imgR,imgR);
  cleanerPGM(imgG,imgG);
  cleanerPGM(imgB,imgB);

  for(int i = 0; i < imgR->getSize();i++)
    {
      imgOut->setColor(i,Color(imgR->get(i,0),imgG->get(i,0),imgB->get(i,0)));
    }
}

void contour(ImageBase* imgIn, ImageBase* imgOut)
{
  cout << "Detourage" << endl;

  ImageBase* imgSeuil = new ImageBase(imgIn->getWidth(), imgIn->getHeight(),imgIn->getColor());
  seuillagePGM(imgIn,imgSeuil);

  ImageBase* imgDil = new ImageBase(imgIn->getWidth(), imgIn->getHeight(),imgIn->getColor());
  morphoBin(imgSeuil,imgDil,false,false);

 for(int x = 0; x < imgSeuil->getHeight(); x++)
   {
       	for(int y = 0; y < imgSeuil->getWidth(); y++)
       	{
	  if(imgSeuil->get(x,y,0) == 255 && imgDil->get(x,y,0) == 255)
	    {
	      imgOut->set(x,y,0,255);
	    }
	  else if(imgSeuil->get(x,y,0) == 0 && imgDil->get(x,y,0) == 0)
	    {
	       imgOut->set(x,y,0,255);
	    }
	  else
	    {
	       imgOut->set(x,y,0,0);
	    }
	}
   }
}

int main(int argc, char **argv)
{
	if (argc != 4) 
	{
		printf("Usage: ImageIn ; ImageOut ;  Question \n   > 0 : erosion\n"); 
		return 1;
	}
	char imageInPath[250] , imageOutPath[250];int question;
	sscanf (argv[1],"%s",imageInPath) ;
	sscanf (argv[2],"%s",imageOutPath) ;
	sscanf (argv[3],"%d",&question) ;
	cout << endl;

	cout << "Loading image " << imageInPath << endl;

	ImageBase image; image.load(imageInPath);
	cout << " > Size : " << image.getSize() << " | " << (image.getColor() ? "PPM" : "PGM") << endl;
	
	ImageBase* imgOut = new ImageBase(image.getWidth(), image.getHeight(),image.getColor());

	switch(question)
	{
		default:return 0;
	case 0:morphoBin(&image,imgOut,true,true); break;
	case 1:morphoBin(&image,imgOut,false,false); break;
	case 2:doubleMorphoBin(&image,imgOut,true);break;
	case 3:doubleMorphoBin(&image,imgOut,false);break;
	case 4:cleanerBin(&image,imgOut);break;
	case 5:contour(&image,imgOut);break;
	case 6: morphoPGM(&image,imgOut,true);break;
	case 7: morphoPGM(&image,imgOut,false);break;
	case 8: cleanerPGM(&image,imgOut);break;
	case 9: cleanerPPM(&image,imgOut);break;
	}

	cout << "Saving" << endl;
	
	imgOut->save(imageOutPath);
	
	return 0;
}

/*
///////////////////////////////////////// Exemple d'un seuillage d'image
	char cNomImgLue[250], cNomImgEcrite[250];
	int S;
  
	if (argc != 4) 
	{
		printf("Usage: ImageIn.pgm ImageOut.pgm Seuil \n"); 
		return 1;
	}
	sscanf (argv[1],"%s",cNomImgLue) ;
	sscanf (argv[2],"%s",cNomImgEcrite);
	sscanf (argv[3],"%d",&S);
	
	
	//ImageBase imIn, imOut;
	ImageBase imIn;
	imIn.load(cNomImgLue);

	//ImageBase imG(imIn.getWidth(), imIn.getHeight(), imIn.getColor());
	ImageBase imOut(imIn.getWidth(), imIn.getHeight(), imIn.getColor());

	for(int x = 0; x < imIn.getHeight(); ++x)
		for(int y = 0; y < imIn.getWidth(); ++y)
		{
			if (imIn[x][y] < S) 
				imOut[x][y] = 0;
			else imOut[x][y] = 255;
		}
		
	imOut.save(cNomImgEcrite);
		

	
	
	///////////////////////////////////////// Exemple de cr�ation d'une image couleur
	ImageBase imC(50, 100, true);

	for(int y = 0; y < imC.getHeight(); ++y)
		for(int x = 0; x < imC.getWidth(); ++x)
		{
			imC[y*3][x*3+0] = 200; // R
			imC[y*3][x*3+1] = 0; // G
			imC[y*3][x*3+2] = 0; // B
		}
		
	imC.save("imC.ppm");
		



	///////////////////////////////////////// Exemple de cr�ation d'une image en niveau de gris
	ImageBase imG(50, 100, false);

	for(int y = 0; y < imG.getHeight(); ++y)
		for(int x = 0; x < imG.getWidth(); ++x)
			imG[y][x] = 50;

	imG.save("imG.pgm");




	ImageBase imC2, imG2;
	
	///////////////////////////////////////// Exemple lecture image couleur
	imC2.load("imC.ppm");
	///////////////////////////////////////// Exemple lecture image en niveau de gris
	imG2.load("imG.pgm");
	
	

	///////////////////////////////////////// Exemple de r�cup�ration d'un plan de l'image
	ImageBase *R = imC2.getPlan(ImageBase::PLAN_R);
	R->save("R.pgm");
	delete R;
	*/

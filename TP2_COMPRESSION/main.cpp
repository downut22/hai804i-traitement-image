#include "../Library/ImageBase.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <cmath>

using namespace std;

ImageBase* assemble(ImageBase* R, ImageBase* G, ImageBase* B, bool YCrCb)
{
	ImageBase* img = new ImageBase(R->getWidth(),R->getHeight(),true);

	for(int x = 0; x < img->getWidth(); x++)
	{
		for(int y = 0; y < img->getHeight(); y++)
		{
		  if(YCrCb)
		    {
		      Color c; c.fromYCRCB(R->get(x,y,0),G->get(x,y,1),B->get(x,y,2));
		      img->setColor(x,y,c);
		    }
		  else
		    {
			img->set(x,y,0,R->get(x,y,0));
			img->set(x,y,1,G->get(x,y,1));
			img->set(x,y,2,B->get(x,y,2));
		    }
		}
	}

	return img;
}

ImageBase* scale2(ImageBase* imgIn)
{
	ImageBase* img = new ImageBase(imgIn->getWidth()*2,imgIn->getHeight()*2,false);

	for(int x = 0; x < img->getWidth(); x++)
	{
		for(int y = 0; y < img->getHeight(); y++)
		{
			img->set(x,y,0,imgIn->get(x/2,y/2,0));
		}
	}

	return img;
}

void reduce2RGB(ImageBase imgIn, ImageBase*& imgR, ImageBase*& imgG, ImageBase*& imgB,  int rgb)
{
	int width = imgIn.getWidth(); int height = imgIn.getHeight();

	imgR = new ImageBase(rgb==0 ? width : (width/2),rgb==0 ? height : (height/2), false);
	imgG = new ImageBase(rgb==1 ? width : (width/2),rgb==1 ? height : (height/2), false);
	imgB = new ImageBase(rgb==2 ? width : (width/2),rgb==2 ? height : (height/2), false);

	for(int i = 0; i < imgIn.getSize();i++)
	{
		if(rgb == 0) {imgR->set(i,0,imgIn.get(i,0));}
		else if(rgb == 1) {imgG->set(i,1,imgIn.get(i,1));}
		else {imgB->set(i,2,imgIn.get(i,2));}
	}

	for(int x = 0; x < width/2; x++)
	{
		for(int y = 0; y < height/2; y++)
		{
			int x2 = x*2; int y2 = y*2;

			int r = (imgIn.get(x2,y2,0) + imgIn.get(x2+1,y2,0) + imgIn.get(x2,y2+1,0) + imgIn.get(x2+1,y2+1,0)) / 4 ;
			int g = (imgIn.get(x2,y2,1) + imgIn.get(x2+1,y2,1) + imgIn.get(x2,y2+1,1) + imgIn.get(x2+1,y2+1,1)) / 4 ;
			int b = (imgIn.get(x2,y2,2) + imgIn.get(x2+1,y2,2) + imgIn.get(x2,y2+1,2) + imgIn.get(x2+1,y2+1,2)) / 4 ;

			if(rgb == 0)
			{
				imgG->set(x,y,1,g);
				imgB->set(x,y,2,b);
			}
			else if(rgb == 1)
			{
				imgR->set(x,y,0,r);
				imgB->set(x,y,2,b);
			}
			else
			{
				imgR->set(x,y,0,r);
				imgG->set(x,y,1,g);
			}
		}
	}
}

void reduce2YCrCb(ImageBase imgIn, ImageBase*& imgY, ImageBase*& imgCr, ImageBase*& imgCb,  int ycb)
{
	int width = imgIn.getWidth(); int height = imgIn.getHeight();

	imgY = new ImageBase(ycb==0 ? width : (width/2),ycb==0 ? height : (height/2), false);
	imgCr = new ImageBase(ycb==1 ? width : (width/2),ycb==1 ? height : (height/2), false);
	imgCb = new ImageBase(ycb==2 ? width : (width/2),ycb==2 ? height : (height/2), false);

	for(int i = 0; i < imgIn.getSize();i++)
	{
		Color c = imgIn.readColor(i);

		if(ycb == 0) {imgY->set(i,0,c.getY());}
		else if(ycb == 1) {imgCr->set(i,1,c.getCr());}
		else {imgCb->set(i,2,c.getCb());}
	}

	for(int x = 0; x < width/2; x++)
	{
		for(int y = 0; y < height/2; y++)
		{
			int x2 = x*2; int y2 = y*2;

			int r = (imgIn.get(x2,y2,0) + imgIn.get(x2+1,y2,0) + imgIn.get(x2,y2+1,0) + imgIn.get(x2+1,y2+1,0)) / 4 ;
			int g = (imgIn.get(x2,y2,1) + imgIn.get(x2+1,y2,1) + imgIn.get(x2,y2+1,1) + imgIn.get(x2+1,y2+1,1)) / 4 ;
			int b = (imgIn.get(x2,y2,2) + imgIn.get(x2+1,y2,2) + imgIn.get(x2,y2+1,2) + imgIn.get(x2+1,y2+1,2)) / 4 ;

			Color c = Color(r,g,b);

			if(ycb == 0)
			{
				imgCr->set(x,y,1,c.getCr());
				imgCb->set(x,y,2,c.getCb());
			}
			else if(ycb == 1)
			{
				imgY->set(x,y,0,c.getY());
				imgCb->set(x,y,2,c.getCb());
			}
			else
			{
				imgY->set(x,y,0,c.getY());
				imgCr->set(x,y,1,c.getCr());
			}
		}
	}
}

int main(int argc, char **argv)
{
	srand(std::time(nullptr));

	if (argc != 5)
	{
		printf("Usage: Image In ; Image out ; Canal not compressed (RGB or YcCrCb > 0 1 2) ; Use YcRcBR ?");
		return 1;
	}
	char imageInPath[250] , imageOutPath[250];int rgb; int useYCrCb;
	sscanf (argv[1],"%s",imageInPath) ;
	sscanf (argv[2],"%s",imageOutPath) ;
	sscanf (argv[3],"%d",&rgb) ;
	sscanf (argv[4],"%d",&useYCrCb) ;

	cout << "Loading image " << imageInPath << endl;

	ImageBase image; image.load(imageInPath);
	cout << " > Size : " << image.getSize() << endl;
	
	ImageBase* imgIn = new ImageBase(image.getWidth(), image.getHeight(), true);
	for(int i = 0; i < image.getSize();i++){imgIn->setColor(i,image.readColor(i));}
		
	if(useYCrCb)
	{
		ImageBase* Y;ImageBase* Cr;ImageBase* Cb;

		reduce2YCrCb(image,Y,Cr,Cb,rgb);

		Y->save("Ylow.ppm");Cr->save("Crlow.ppm");Cb->save("Cblow.ppm");

		if(rgb==0)
		{
			Cr = scale2(Cr); Cb = scale2(Cb);
		}
		else if(rgb == 1)
		{
			Y = scale2(Y); Cb = scale2(Cb);
		}
		else
		{
			Y = scale2(Y); Cr = scale2(Cr);
		}

		Y->save("Yhigh.ppm");Cr->save("Crhigh.ppm");Cb->save("Cbhigh.ppm");

		cout << "Assembling YCRCB" << endl;

		ImageBase* imgOut = assemble(Y,Cr,Cb,true);
		
		double psnr = imgOut->PSNR(imgIn);

		cout << "PSNR : " << psnr << endl;
		
		cout << "Saving YCRCB" << endl;

		imgOut->save(imageOutPath);
	}
	else
	{	
		ImageBase* R;ImageBase* G;ImageBase* B;

		reduce2RGB(image,R,G,B,rgb);

		cout << "Saving low resolution" << endl;

		R->save("Rlow.pgm"); G->save("Glow.pgm"); B->save("Blow.pgm");

		if(rgb==0)
		{
			G = scale2(G); B = scale2(B);
		}
		else if(rgb == 1)
		{
			R = scale2(R); B = scale2(B);
		}
		else
		{
			R = scale2(R); G = scale2(G);
		}

		cout << "Saving high resolution" << endl;

		R->save("Rhigh.pgm"); G->save("Ghigh.pgm"); B->save("Bhigh.pgm");

		cout << "Assembling RGB" << endl;

		ImageBase* imgOut = assemble(R,G,B,false);

		double psnr = imgOut->PSNR(imgIn);

		cout << "PSNR : " << psnr << endl;
		
		cout << "Saving RGB" << endl;

		imgOut->save(imageOutPath);
	}

	cout << "END" << endl;

	return 0;
}


//Y CR CB 34.489 31.5443 30.0544
//R G B 34.9386 31.5049 31.1803

#include "../Library/ImageBase.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

using namespace std;

ImageBase* getGrey(ImageBase* image)
{
	ImageBase* img = new ImageBase(image->getWidth(), image->getHeight(),false);

	for(int i = 0; i < image->getSize();i++)
	{
		img->set(i,0,image->readColor(i).getY());
	}

	return img;
}

void gradientMap(ImageBase* image, ImageBase* imgOut)
{
	for(int x = 0; x < image->getWidth();x++)
	{
		for(int y = 0; y < image->getHeight();y++)
		{
			int v = 0; int h = 0; int p = image->get(x,y,0);

			if(x > 0){h += p - image->get(x-1,y,0);}
			if(x < image->getWidth()-1){ h += p - image->get(x+1,y,0);}
			if(y > 0){v += p - image->get(x,y-1,0);}
			if(y < image->getHeight()-1){ h += p - image->get(x,y+1,0);}

			int n = (int)sqrt((v*v)+(h*h));

			n = max(0,min(255,n));
			imgOut->set(x,y,0,n);
		}
	}
}

void seuillagePGM(ImageBase* imgIn, ImageBase* imgOut, int seuil)
{
	for(int x = 0; x < imgIn->getHeight(); ++x)
	{
		for(int y = 0; y < imgIn->getWidth(); y++)
		{
		  imgOut->set(x,y,0,imgIn->get(x,y,0) > seuil ? 255 : 0);
		}
	}
}

void hysteresisPGM(ImageBase* imgIn, ImageBase* imgOut, int sb, int sh)
{
	ImageBase* imgTemp = new ImageBase(imgIn->getWidth(), imgIn->getHeight(),false);

	for(int x = 0; x < imgIn->getHeight(); ++x)
	{
		for(int y = 0; y < imgIn->getWidth(); y++)
		{
			if(imgIn->get(x,y,0) > sh)
			{
				imgTemp->set(x,y,0,255);
			}
			else if(imgIn->get(x,y,0) < sb)
			{
				imgTemp->set(x,y,0,0);
			}
			else 
			{
				imgTemp->set(x,y,0,imgIn->get(x,y,0) );
			}
		}
	}
	
	int voisinsX[9] = {1,1,1,0,0,0,-1,-1,-1};
	int voisinsY[9] = {1,0,-1,1,0,-1,1,0,-1};

	for(int x = 0; x < imgIn->getHeight(); ++x)
	{
		for(int y = 0; y < imgIn->getWidth(); y++)
		{
			if(imgTemp->get(x,y,0) <= sh && imgTemp->get(x,y,0) >= sb)
			{
				bool voisinContour = false;
				for(int i = 0; i < 9; i++)
				{
					if(x + voisinsX[i] < 0 || x + voisinsX[i] >= imgOut->getWidth()){continue;}
					if(y + voisinsY[i] < 0 || y + voisinsY[i] >= imgOut->getHeight()){continue;}

					if(imgTemp->get(x + voisinsX[i],y + voisinsY[i],0) >= 255)
					{
						voisinContour = true; break;
					}
				}
				if(voisinContour)
				{
					imgOut->set(x,y,0,255);
				}
				else
				{
					imgOut->set(x,y,0,0);
				}
			}
			else
			{
				imgOut->set(x,y,0,imgTemp->get(x,y,0));
			}
		}
	}
}

void blur(ImageBase* image,ImageBase* imgOut)
{
	int voisinsX[9] = {1,1,1,0,0,0,-1,-1,-1};
	int voisinsY[9] = {1,0,-1,1,0,-1,1,0,-1};

	for(int x = 0; x < image->getWidth(); x++)
	{
		for(int y = 0; y < image->getHeight(); y++)
		{
			double r = 0; double g = 0; double b = 0; int c = 0;
			for(int i = 0; i < 9; i++)
			{
				if(x + voisinsX[i] < 0 || x + voisinsX[i] >= image->getWidth()){continue;}
				if( y + voisinsY[i] < 0 || y + voisinsY[i] >= image->getHeight()){continue;}

				r += image->get(x + voisinsX[i], y + voisinsY[i],0);
				g += image->get(x + voisinsX[i], y + voisinsY[i],1);
				b += image->get(x + voisinsX[i], y + voisinsY[i],2);
				c++;
			}
			r /= c; g /= c; b /= c;
			imgOut->set(x,y,0,(unsigned char)r);
			imgOut->set(x,y,1,(unsigned char)g);
			imgOut->set(x,y,2,(unsigned char)b);
		}
   }
}

void laplaceMap(ImageBase* image, ImageBase* imgOut)
{
	for(int x = 0; x < image->getWidth();x++)
	{
		for(int y = 0; y < image->getHeight();y++)
		{
			int p = image->get(x,y,0) * 4;

			if(x > 0){ p += image->get(x-1,y,0) * -1;}
			if(x < image->getWidth()-1){ p += image->get(x+1,y,0) * -1;}
			if(y > 0){ p += image->get(x,y-1,0) * -1;}
			if(y < image->getHeight()-1){ p += image->get(x,y+1,0) * -1;}

			p += 128; // max(0,min(255,p));
			imgOut->set(x,y,0,p);
		}
	}
}

void passageZero(ImageBase* image, ImageBase* imgOut)
{
	int xps[8] = {1,1,0,-1,-1,-1,0,1};
	int yps[8] = {0,1,1,1,0,-1,-1,-1};

	for(int x = 0; x < image->getWidth();x++)
	{
		for(int y = 0; y < image->getHeight();y++)
		{
			if(x >= image->getWidth()-1 || x <= 0) {imgOut->set(x,y,0,0); continue;}
			if(y >= image->getHeight()-1 || y <= 0) {imgOut->set(x,y,0,0); continue;}

			int a = 0; int b = 0;

			if(x < image->getWidth()-1)
			{ 
				a = (image->get(x+1,y,0)-128) - (image->get(x,y,0)-128);  
			}
			if(y < image->getHeight()-1)
			{ 
				b = (image->get(x,y+1,0)-128) - (image->get(x,y,0)-128);  
			}

			if(abs(b) <= 0){imgOut->set(x,y,0,0); continue;}

			float d = atan(abs(a)/(float)abs(b));
			int di = (int)((d / (M_PI*2))*7);
			int xp = xps[di]; int yp = yps[di];

			int g = 0;
			if( (image->get(x,y,0) > 0 & image->get(xp,yp,0) < 0) || (image->get(x,y,0) < 0 & image->get(xp,yp,0) > 0 ))
			{
				g = image->get(x,y,0);//max(abs(a),abs(b)) * 255;
				cout << "zero" << endl;
			}

			imgOut->set(x,y,0,g);
		}
	}
}

int main(int argc, char **argv)
{
	if (argc != 3)
	{
		printf("Usage: ImageIn ; ImageOut ;\n");
		return 1;
	}
	char imageInPath[250] , imageOutPath[250];
	sscanf (argv[1],"%s",imageInPath) ;
	sscanf (argv[2],"%s",imageOutPath) ;

	cout << endl;

	cout << "Loading image " << imageInPath << endl;

	ImageBase* image = new ImageBase(); image->load(imageInPath);
	cout << " > Size : " << image->getSize() << " | " << (image->getColor() ? "PPM" : "PGM") << endl;
	
	int* lineProfile = image->lineProfile(0,50);
	image->saveProfile(lineProfile,image->getWidth(),"profile_base.dat");

	ImageBase* imgBlur = new ImageBase(image->getWidth(), image->getHeight(),false);
ImageBase* imgOut = new ImageBase(image->getWidth(), image->getHeight(),false);


	cout << "Bluring" << endl;
	blur(image,imgBlur);
	imgBlur->save("blur.pgm");

	lineProfile = imgBlur->lineProfile(0,50);
	imgBlur->saveProfile(lineProfile,imgBlur->getWidth(),"profile_blur.dat");


	ImageBase* imgLap = new ImageBase(image->getWidth(), image->getHeight(),false);

	cout << "Generating laplace map" << endl;
	laplaceMap(imgBlur,imgLap);
	cout << "Saving laplace" << endl;
	imgLap->save("laplace_base.pgm");

	
	cout << "Generating passageZero map" << endl;
	passageZero(imgLap,imgOut);
	cout << "Saving passageZero" << endl;
	imgOut->save("zeros.pgm");
	/*
	cout << "Generating gradient map" << endl;
	gradientMap(image,imgOut);
	cout << "Saving gradient" << endl;
	imgOut->save("gradient_base.pgm");

	cout << "Saving line profile" << endl;
	lineProfile = imgOut->lineProfile(0,50);
	imgOut->saveProfile(lineProfile,imgOut->getWidth(),"profile.dat");

	cout << "Seuilling map" << endl;
	//seuillagePGM(imgOut,imgOut,125);
	hysteresisPGM(imgOut,imgOut,100,125);
	cout << "Saving gradient seuilled" << endl;
	imgOut->save("gradient_seuil.pgm"); */

	cout << "Saving final" << endl;
	imgOut->save(imageOutPath);

	return 0;
}

/*
///////////////////////////////////////// Exemple d'un seuillage d'image
	char cNomImgLue[250], cNomImgEcrite[250];
	int S;
  
	if (argc != 4) 
	{
		printf("Usage: ImageIn.pgm ImageOut.pgm Seuil \n"); 
		return 1;
	}
	sscanf (argv[1],"%s",cNomImgLue) ;
	sscanf (argv[2],"%s",cNomImgEcrite);
	sscanf (argv[3],"%d",&S);
	
	
	//ImageBase imIn, imOut;
	ImageBase imIn;
	imIn.load(cNomImgLue);

	//ImageBase imG(imIn.getWidth(), imIn.getHeight(), imIn.getColor());
	ImageBase imOut(imIn.getWidth(), imIn.getHeight(), imIn.getColor());

	for(int x = 0; x < imIn.getHeight(); ++x)
		for(int y = 0; y < imIn.getWidth(); ++y)
		{
			if (imIn[x][y] < S) 
				imOut[x][y] = 0;
			else imOut[x][y] = 255;
		}
		
	imOut.save(cNomImgEcrite);
		

	
	
	///////////////////////////////////////// Exemple de cr�ation d'une image couleur
	ImageBase imC(50, 100, true);

	for(int y = 0; y < imC.getHeight(); ++y)
		for(int x = 0; x < imC.getWidth(); ++x)
		{
			imC[y*3][x*3+0] = 200; // R
			imC[y*3][x*3+1] = 0; // G
			imC[y*3][x*3+2] = 0; // B
		}
		
	imC.save("imC.ppm");
		



	///////////////////////////////////////// Exemple de cr�ation d'une image en niveau de gris
	ImageBase imG(50, 100, false);

	for(int y = 0; y < imG.getHeight(); ++y)
		for(int x = 0; x < imG.getWidth(); ++x)
			imG[y][x] = 50;

	imG.save("imG.pgm");




	ImageBase imC2, imG2;
	
	///////////////////////////////////////// Exemple lecture image couleur
	imC2.load("imC.ppm");
	///////////////////////////////////////// Exemple lecture image en niveau de gris
	imG2.load("imG.pgm");
	
	

	///////////////////////////////////////// Exemple de r�cup�ration d'un plan de l'image
	ImageBase *R = imC2.getPlan(ImageBase::PLAN_R);
	R->save("R.pgm");
	delete R;
	*/

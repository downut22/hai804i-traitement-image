#include "../Library/ImageBase.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <climits>
#include <string>

using namespace std;

int sizeX,sizeY,sizeZ,sizer;
float voxelSizeX,voxelSizeY,voxelSizeZ;
unsigned short* datar;

unsigned short threshold;

unsigned short getValue(int i, int j , int k)
{
	return datar[i + (j*sizeX) + (k*sizeX*sizeY)];
}

unsigned short getMaxValue(unsigned short* array,int arraysize)
{
	unsigned short v = 0;

	for(int i = 0; i < arraysize; i++)
	{
		v = max(v,array[i]);
	}

	return v;
}

unsigned short getMinValue(unsigned short* array,int arraysize)
{
	unsigned short v = USHRT_MAX;

	for(int i = 0; i < arraysize; i++)
	{
		v = min(v,array[i]);
	}

	return v;
}

struct Point
{
	float X,Y,Z;
	Point(float _x, float _y, float _z)
	{
		X = _x; Y = _y; Z = _z;
	}
};

struct Triangle
{
	int v1,v2,v3;
	Triangle(int _v1, int _v2, int _v3)
	{
		v1 = _v1; v2 = _v2; v3 = _v3;
	}
};

struct RenderData
{
	vector<Point*> points;
	vector<Triangle*> triangles;

	void addTriangle(Point* v1, Point* v2, Point* v3)
	{
		points.push_back(v1);
		points.push_back(v2);
		points.push_back(v3);
		triangles.push_back(new Triangle(points.size()-3,points.size()-2,points.size()-1));
	}
};

void addTriangles(RenderData* renderData,int x,int y,int z,int face)
{
	Point* v1 = new Point((x-0.5)*voxelSizeX, (y-0.5) * voxelSizeY,(z-0.5)*voxelSizeZ);
	Point* v2 = new Point((x-0.5)*voxelSizeX, (y-0.5) * voxelSizeY,(z+0.5)*voxelSizeZ);
	Point* v3 = new Point((x-0.5)*voxelSizeX, (y+0.5) * voxelSizeY,(z+0.5)*voxelSizeZ);
	Point* v4 = new Point((x-0.5)*voxelSizeX, (y+0.5) * voxelSizeY,(z-0.5)*voxelSizeZ);
	Point* v5 = new Point((x+0.5)*voxelSizeX, (y-0.5) * voxelSizeY,(z-0.5)*voxelSizeZ);
	Point* v6 = new Point((x+0.5)*voxelSizeX, (y-0.5) * voxelSizeY,(z+0.5)*voxelSizeZ);
	Point* v7 = new Point((x+0.5)*voxelSizeX, (y+0.5) * voxelSizeY,(z+0.5)*voxelSizeZ);
	Point* v8 = new Point((x+0.5)*voxelSizeX, (y+0.5) * voxelSizeY,(z-0.5)*voxelSizeZ);

	switch(face)
	 {
	 	case 0: renderData->addTriangle(v1,v2,v3); renderData->addTriangle(v3,v4,v1); break;
	 	case 1: renderData->addTriangle(v5,v6,v7); renderData->addTriangle(v7,v8,v5); break;

	 	case 2: renderData->addTriangle(v1,v5,v6); renderData->addTriangle(v6,v2,v1); break;
	 	case 3: renderData->addTriangle(v4,v8,v7); renderData->addTriangle(v7,v3,v4); break;

	 	case 4: renderData->addTriangle(v1,v5,v8); renderData->addTriangle(v8,v4,v1); break;
	 	case 5: renderData->addTriangle(v2,v6,v7); renderData->addTriangle(v7,v3,v2); break;
	 }
}

RenderData* render()
{
	RenderData* renderData = new RenderData();

	int gabaritX[6] = {-1	,1	,0	,0	,0	,0};
	int gabaritY[6] = {0	,0	,-1	,1	,0	,0};
	int gabaritZ[6] = {0	,0	,0	,0	,-1	,1};

	for(int x = 1; x < sizeX-1; x++)
	{
		for(int y = 1; y < sizeY-1; y++)
		{
			for(int z = 1; z < sizeZ-1; z++)
			{
				unsigned short val = getValue(x,y,z); 
				if(val >= threshold)
				{
					for(int i = 0; i < 6; i++)
					{
						if(getValue(x+gabaritX[i],y+gabaritY[i],z+gabaritZ[i]) < threshold) 
						{
							addTriangles(renderData,x,y,z,i);
						}
					}
				}
			}
		}
	}

	return renderData;
}

string generateSTL(RenderData* renderData)
{
	string str = "";

	for(int i = 0; i < renderData->triangles.size(); i++)
	{
		str += "facet normal 0 0 0\nouter loop\n";

		str += "vertex ";
		str += to_string(renderData->points.at(renderData->triangles.at(i)->v1)->X); str +=  " ";
		str += to_string(renderData->points.at(renderData->triangles.at(i)->v1)->Y); str +=  " ";
		str += to_string(renderData->points.at(renderData->triangles.at(i)->v1)->Z); str += "\n";
		str += "vertex " ;
		str += to_string(renderData->points.at(renderData->triangles.at(i)->v2)->X); str += " ";
		str += to_string(renderData->points.at(renderData->triangles.at(i)->v2)->Y); str +=  " ";
		str += to_string(renderData->points.at(renderData->triangles.at(i)->v2)->Z); str +=  "\n";
		str += "vertex " ;
		str += to_string(renderData->points.at(renderData->triangles.at(i)->v3)->X); str +=  " ";
		str += to_string(renderData->points.at(renderData->triangles.at(i)->v3)->Y); str += " ";
		str += to_string(renderData->points.at(renderData->triangles.at(i)->v3)->Z); str +=  "\n";

		str += "endloop\nendfacet\n";
	}

	return str;
}

int main(int argc, char **argv)
{
	if (argc != 10)
	{
		printf("Usage: ImageIn ; DimX ; DimY ; DimZ ; SizeX ; SizeY; SizeZ ; output ; threshold\n");
		return 1;
	}
	char imageInPath[250],outPath[250]; 
	sscanf (argv[1],"%s",imageInPath) ;
	sscanf (argv[2],"%d",&sizeX) ;
	sscanf (argv[3],"%d",&sizeY) ;
	sscanf (argv[4],"%d",&sizeZ) ;
	sscanf (argv[5],"%f",&voxelSizeX) ;
	sscanf (argv[6],"%f",&voxelSizeY) ;
	sscanf (argv[7],"%f",&voxelSizeZ) ;
	sscanf (argv[8],"%s",&outPath) ;
	sscanf (argv[9],"%d",&threshold) ;

	cout << endl << "Opening " << imageInPath << " of size X = " << sizeX << " Y = " << sizeY << " Z = " << sizeZ << endl;
	cout << "Voxel size : " << voxelSizeX << " x " << voxelSizeY << " x " << voxelSizeZ << endl;
 
	sizer = sizeX*sizeY*sizeZ;
	datar = new unsigned short[sizer];

	unsigned char* datac = new unsigned char[sizer*2];

	FILE* file = fopen(imageInPath,"rb");
	int readed = fread(datac,sizeof(unsigned char),sizer*2,file);

	cout << "Successfully read " << readed << " char values" << endl;

	for(int i = 0; i < sizer*2; i+=2)
	{
		datar[i/2] = (unsigned short)datac[i+1] + ((unsigned short)datac[i]*256);
	}

	unsigned short maxValue = getMaxValue(datar,sizer);
	unsigned short minValue = getMinValue(datar,sizer);

	cout << "Max value " << maxValue << "   Min value " << minValue << endl;

	RenderData* renderData = render();

	string stldata = generateSTL(renderData);

	ofstream flux(outPath);
	flux << stldata;

	return 0;
}
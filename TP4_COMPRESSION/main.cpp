#include "../Library/ImageBase.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <cmath>

using namespace std;

void RGBtoYCrCb(ImageBase* imgIn, ImageBase* imgOut)
{
	for(int i = 0; i < imgIn->getSize();i++)
	{
		Color c = imgIn->readColor(i);

		imgOut->set(i,0,c.getY());
		imgOut->set(i,1,c.getCr());
		imgOut->set(i,2,c.getCb());
	}
}

void assemble(ImageBase* bf, ImageBase* mfh, ImageBase* mfv, ImageBase* hf,ImageBase*& imgOut)
{
	imgOut = new ImageBase(bf->getWidth() * 2, bf->getHeight() * 2, false);

	int halfWidth = imgOut->getWidth() / 2;
	int halfHeight = imgOut->getHeight() / 2;

	for(int y = 0; y < imgOut->getHeight(); y++)
	{
		for(int x = 0; x < imgOut->getWidth(); x++)
		{
			if(x < halfWidth)
			{
				if(y < halfHeight)
				{
					imgOut->set(x,y,0,bf->get(x/2,y/2,0));
				}
				else
				{
					imgOut->set(x,y,0,mfh->get(x/2,y/2 - halfHeight,0));
				}
			}
			else
			{
				if(y < halfHeight)
				{
					imgOut->set(x,y,0,mfv->get(x/2 - halfWidth,y/2,0));
				}
				else
				{
					imgOut->set(x,y,0,hf->get(x/2 - halfWidth,y/2 - halfHeight,0));
				}
			}
		}
	}
}

void decompose(ImageBase* imgIn, ImageBase*& bf, ImageBase*& mfh, ImageBase*& mfv, ImageBase*& hf)
{
	bf = new ImageBase(imgIn->getWidth() / 2, imgIn->getHeight() / 2,false);
	mfh = new ImageBase(imgIn->getWidth() / 2, imgIn->getHeight() / 2,false);
	mfv = new ImageBase(imgIn->getWidth() / 2, imgIn->getHeight() / 2,false);
	hf = new ImageBase(imgIn->getWidth() / 2, imgIn->getHeight() / 2,false);

	for(int y = 0; y < imgIn->getHeight(); y+=2)
	{
		for(int x = 0; x < imgIn->getWidth(); x+=2)
		{
			int a = imgIn->get(x,y,0);
			int b = imgIn->get(x+1,y,0);
			int c = imgIn->get(x,y+1,0);
			int d = imgIn->get(x+1,y+1,0);

			bf->set(x/2,y/2,0,(a+b+c+d)/4);
			mfh->set(x/2,y/2,0,(a+b-c-d)/2 + 128);
			mfv->set(x/2,y/2,0,(a-b+c-d)/4 + 128);
			hf->set(x/2,y/2,0,a-b-c+d + 128);
		}
	}
}

void recompose(ImageBase* bf, ImageBase* mfh, ImageBase* mfv, ImageBase* hf,ImageBase*& imgOut)
{
	imgOut = new ImageBase(bf->getWidth() * 2, bf->getHeight() * 2, false);


	for(int y = 0; y < imgOut->getHeight(); y++)
	{
		for(int x = 0; x < imgOut->getWidth(); x++)
		{
			int bf_v = bf->get(x/2,y/2,0);
			int mfv_v = mfv->get(x/2,y/2,0);
			int mfh_v = mfh->get(x/2,y/2,0);
			int hf_v = hf->get(x/2,y/2,0);
		}
	}
}

int main(int argc, char **argv)
{
	srand(std::time(nullptr));

	if (argc != 3)
	{
		printf("Usage: Image In ; Image out ");
		return 1;
	}
	char imageInPath[250], imageOutPath[250];
	sscanf (argv[1],"%s",imageInPath) ;
	sscanf (argv[2],"%s",imageOutPath) ;

	ImageBase* image = new ImageBase(); image->load(imageInPath);
	cout << " > Size : " << image->getSize()  << 
	" | entropy :  R : " << image->entropy(0) << 
	"  G : " << image->entropy(1) <<
	"  B : " << image->entropy(2) << endl;


	ImageBase* bf;ImageBase* mfh; ImageBase* mfv; ImageBase* hf;
	decompose(image,bf,mfh,mfv,hf);

	ImageBase* imgOut;
	assemble(bf,mfh,mfv,hf,imgOut);

	bf->save("bf.pgm");
	mfh->save("mfh.pgm");
	mfv->save("mfv.pgm");
	hf->save("hf.pgm");

	imgOut->save(imageOutPath);
	
	cout << "END" << endl;

	return 0;
}

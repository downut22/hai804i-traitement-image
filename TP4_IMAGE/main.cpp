#include "../Library/ImageBase.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

using namespace std;

ImageBase* getGrey(ImageBase* image)
{
	ImageBase* img = new ImageBase(image->getWidth(), image->getHeight(),false);

	for(int i = 0; i < image->getSize();i++)
	{
		img->set(i,0,image->readColor(i).getY());
	}

	return img;
}

void seuillagePGM(ImageBase* imgIn, ImageBase* imgOut)
{
  cout<< " > Seuillage PGM" <<endl;
  cout<< "  > Seuil : ";
  int seuil;scanf("%i",&seuil);

        for(int x = 0; x < imgIn->getHeight(); ++x)
	{
		for(int y = 0; y < imgIn->getWidth(); y++)
		{
		  imgOut->set(x,y,0,imgIn->get(x,y,0) > seuil ? 255 : 0);
		}
	}
}

void blur(ImageBase* image,ImageBase* imgOut)
{
	int voisinsX[9] = {1,1,1,0,0,0,-1,-1,-1};
	int voisinsY[9] = {1,0,-1,1,0,-1,1,0,-1};

	for(int x = 0; x < image->getWidth(); x++)
	{
		for(int y = 0; y < image->getHeight(); y++)
		{
			double r = 0; double g = 0; double b = 0; int c = 0;
			for(int i = 0; i < 9; i++)
			{
				if(x + voisinsX[i] < 0 || x + voisinsX[i] >= image->getWidth()){continue;}
				if( y + voisinsY[i] < 0 || y + voisinsY[i] >= image->getHeight()){continue;}

				r += image->get(x + voisinsX[i], y + voisinsY[i],0);
				g += image->get(x + voisinsX[i], y + voisinsY[i],1);
				b += image->get(x + voisinsX[i], y + voisinsY[i],2);
				c++;
			}
			r /= c; g /= c; b /= c;
			imgOut->set(x,y,0,(unsigned char)r);
			imgOut->set(x,y,1,(unsigned char)g);
			imgOut->set(x,y,2,(unsigned char)b);
		}
   }
}

void blurBackground(ImageBase* image, ImageBase* calque,ImageBase* imgOut)
{
	int voisinsX[9] = {1,1,1,0,0,0,-1,-1,-1};
	int voisinsY[9] = {1,0,-1,1,0,-1,1,0,-1};

	for(int x = 0; x < image->getHeight(); x++)
	{
		for(int y = 0; y < image->getWidth(); y++)
		{
			if(calque->get(x,y,0) <= 0)
			{
				double r = 0; double g = 0; double b = 0; int c = 0;
				for(int i = 0; i < 9; i++)
				{
					if(x + voisinsX[i] < 0 || x + voisinsX[i] >= image->getWidth()){continue;}
					if( y + voisinsY[i] < 0 || y + voisinsY[i] >= image->getHeight()){continue;}
					if(calque->get(x + voisinsX[i], y + voisinsY[i],0) >= 200){continue;}

					r += image->get(x + voisinsX[i], y + voisinsY[i],0);
					g += image->get(x + voisinsX[i], y + voisinsY[i],1);
					b += image->get(x + voisinsX[i], y + voisinsY[i],2);
					c++;
				}
				r /= c; g /= c; b /= c;
				imgOut->set(x,y,0,(unsigned char)r);
				imgOut->set(x,y,1,(unsigned char)g);
				imgOut->set(x,y,2,(unsigned char)b);
			}
			else
			{
				imgOut->setColor(x,y,image->readColor(x,y));
			}
		}
   }
}


void morphoBin(ImageBase* imgSeuil, ImageBase* imgOut,bool erosion) //or dilatation
{
  cout << (erosion ? "Erosion binaire" : "Dilatation binaire") << endl;

  int voisinsX[4] = {1,0,-1,0};
  int voisinsY[4] = {0,1,0,-1};

   for(int x = 0; x < imgSeuil->getHeight(); x++)
   {
       	for(int y = 0; y < imgSeuil->getWidth(); y++)
       	{
	  bool hasVoisin = false;
	  for(int i = 0; i < 4; i++)
	    {
	      if(x + voisinsX[i] < 0 || x + voisinsX[i] >= imgSeuil->getWidth()){continue;}
	      if(y + voisinsY[i] < 0 || y + voisinsY[i] >= imgSeuil->getWidth()){continue;}

	      if(erosion && imgSeuil->get(x + voisinsX[i] , y + voisinsY[i],0) > 0){hasVoisin = true; break;}
	      if(!erosion && imgSeuil->get(x + voisinsX[i] , y + voisinsY[i],0) < 255){hasVoisin = true; break;}
	    }

	  if(erosion)
	    imgOut->set(x,y,0,hasVoisin ? 255 : (imgSeuil->get(x,y,0)));
	  else
	    imgOut->set(x,y,0,hasVoisin ? 0 : (imgSeuil->get(x,y,0)));
       	}
   }
}

void cleanerBin(ImageBase* imgIn, ImageBase* imgOut)
{
  cout << "Cleaner" << endl;

  bool operations[12] = {false,false,false,true,true,true,true,true,true,false,false,false};

  ImageBase* previous;
  for(int i = 0; i < 12; i++)
    {
      ImageBase* imgTemp = new ImageBase(imgIn->getWidth(), imgIn->getHeight(),imgIn->getColor());

      morphoBin(i == 0 ? imgIn : previous,i==11 ? imgOut : imgTemp,operations[i]);
      previous = imgTemp;
    }
}

int main(int argc, char **argv)
{
	if (argc != 3)
	{
		printf("Usage: ImageIn ; ImageOut ;\n");
		return 1;
	}
	char imageInPath[250] , imageOutPath[250];
	sscanf (argv[1],"%s",imageInPath) ;
	sscanf (argv[2],"%s",imageOutPath) ;

	cout << endl;

	cout << "Loading image " << imageInPath << endl;

	ImageBase* image = new ImageBase(); image->load(imageInPath);
	cout << " > Size : " << image->getSize() << " | " << (image->getColor() ? "PPM" : "PGM") << endl;
	
	cout << "Creating grey image" << endl;
	ImageBase* greyImage = getGrey(image);
	cout << "Saving grey image" << endl;
	greyImage->save("grey.pgm");
	cout << "Saving grey histogram" << endl;
	greyImage->saveHistogram(greyImage->histogram(0),"histo.dat");

	seuillagePGM(greyImage,greyImage);
	cout << "Saving grey image" << endl;
	greyImage->save("seuil.pgm");

	cleanerBin(greyImage,greyImage);
	cout <<"Saving cleaned grey image" << endl;
	greyImage->save("calque.pgm");

	ImageBase* imgOut = new ImageBase(image->getWidth(), image->getHeight(),image->getColor());

	cout << "Bluring image" << endl;
	blur(image,imgOut);
	cout << "Saving blur" << endl;
	imgOut->save("blur.ppm");

	cout << "Bluring background" << endl;
	blurBackground(image,greyImage,imgOut);
	cout << "Saving final" << endl;
	imgOut->save(imageOutPath);

	return 0;
}

/*
///////////////////////////////////////// Exemple d'un seuillage d'image
	char cNomImgLue[250], cNomImgEcrite[250];
	int S;
  
	if (argc != 4) 
	{
		printf("Usage: ImageIn.pgm ImageOut.pgm Seuil \n"); 
		return 1;
	}
	sscanf (argv[1],"%s",cNomImgLue) ;
	sscanf (argv[2],"%s",cNomImgEcrite);
	sscanf (argv[3],"%d",&S);
	
	
	//ImageBase imIn, imOut;
	ImageBase imIn;
	imIn.load(cNomImgLue);

	//ImageBase imG(imIn.getWidth(), imIn.getHeight(), imIn.getColor());
	ImageBase imOut(imIn.getWidth(), imIn.getHeight(), imIn.getColor());

	for(int x = 0; x < imIn.getHeight(); ++x)
		for(int y = 0; y < imIn.getWidth(); ++y)
		{
			if (imIn[x][y] < S) 
				imOut[x][y] = 0;
			else imOut[x][y] = 255;
		}
		
	imOut.save(cNomImgEcrite);
		

	
	
	///////////////////////////////////////// Exemple de cr�ation d'une image couleur
	ImageBase imC(50, 100, true);

	for(int y = 0; y < imC.getHeight(); ++y)
		for(int x = 0; x < imC.getWidth(); ++x)
		{
			imC[y*3][x*3+0] = 200; // R
			imC[y*3][x*3+1] = 0; // G
			imC[y*3][x*3+2] = 0; // B
		}
		
	imC.save("imC.ppm");
		



	///////////////////////////////////////// Exemple de cr�ation d'une image en niveau de gris
	ImageBase imG(50, 100, false);

	for(int y = 0; y < imG.getHeight(); ++y)
		for(int x = 0; x < imG.getWidth(); ++x)
			imG[y][x] = 50;

	imG.save("imG.pgm");




	ImageBase imC2, imG2;
	
	///////////////////////////////////////// Exemple lecture image couleur
	imC2.load("imC.ppm");
	///////////////////////////////////////// Exemple lecture image en niveau de gris
	imG2.load("imG.pgm");
	
	

	///////////////////////////////////////// Exemple de r�cup�ration d'un plan de l'image
	ImageBase *R = imC2.getPlan(ImageBase::PLAN_R);
	R->save("R.pgm");
	delete R;
	*/

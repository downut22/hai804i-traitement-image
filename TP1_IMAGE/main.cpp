#include "../Library/ImageBase.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

void seuillagePGM()
{
	std::cout << "   -- Seuillage PGM avec un seul niveau" << std::endl;

	char imageInPath[250] , imageOutPath[250], thresholdChar[250];
	int threshold;

	std::cout << "Input Image : "; std::cin >> imageInPath; std::cout << std::endl;
	std::cout << "Output Image : "; std::cin >> imageOutPath; std::cout << std::endl;
	std::cout << "Threshold : "; std::cin >> thresholdChar; std::cout << std::endl; sscanf (thresholdChar,"%d",&threshold);

	std::cout << std::endl << imageInPath << " >> " << imageOutPath << " : " << threshold << std::endl;

	ImageBase imgIn; imgIn.load(imageInPath);

	ImageBase imgOut(imgIn.getWidth(), imgIn.getHeight(), false);

	for(int x = 0; x < imgIn.getHeight(); ++x)
	{
		for(int y = 0; y < imgIn.getWidth(); ++y)
		{
			if (imgIn[x][y] < threshold) 
			{
				imgOut[x][y] = 0;
			}
			else 
			{
				imgOut[x][y] = 255;
			}
		}
	}

	std::cout << std::endl << "Saving image" << std::endl;

	imgOut.save(imageOutPath);
}

void seuillageMultiplePGM()
{
	std::cout << "   -- Seuillage PGM avec plusieurs niveaux" << std::endl;

	char imageInPath[250] , imageOutPath[250], thresholdChar[250];
	int thresholdCount;

	std::cout << "Input Image : "; std::cin >> imageInPath; std::cout << std::endl;
	std::cout << "Output Image : "; std::cin >> imageOutPath; std::cout << std::endl;
	std::cout << "Threshold count : "; std::cin >> thresholdChar; std::cout << std::endl; sscanf (thresholdChar,"%d",&thresholdCount);

	std::cout << "   > Entrez les " << thresholdCount << " niveaux de seuillage : " << std::endl;

	int* thresholds = new int[thresholdCount];

	for(int i = 0; i < thresholdCount ; i++)
	{
		std::cin >> thresholdChar; 
		sscanf (thresholdChar,"%d",&thresholds[i]);
	}

	std::cout << std::endl << imageInPath << " >> " << imageOutPath << " : ";
	for(int i = 0; i < thresholdCount ; i++){std::cout << thresholds[i] << " ";}
	std::cout << std::endl;

	ImageBase imgIn; imgIn.load(imageInPath);

	ImageBase imgOut(imgIn.getWidth(), imgIn.getHeight(), false);

	for(int x = 0; x < imgIn.getHeight(); ++x)
	{
		for(int y = 0; y < imgIn.getWidth(); ++y)
		{
			imgOut[x][y] = -1;
		}
	}

	int step = (int)(255.0/thresholdCount);
	for(int x = 0; x < imgIn.getHeight(); ++x)
	{
		for(int y = 0; y < imgIn.getWidth(); ++y)
		{
			for(int i = thresholdCount-1 ; i >= 0; i--)
			{
				if(imgIn[x][y] < thresholds[i])
				{
					imgOut[x][y] = step * i;
				}
			}
			if(imgOut[x][y] == -1) { imgOut[x][y] = 255;}
		}
	}

	std::cout << std::endl << "Saving image" << std::endl;

	imgOut.save(imageOutPath);
}

void profilPGM()
{
	std::cout << "   -- Profil d'un ligne" << std::endl;

	char imageInPath[250] , dataPath[250] , lineChar[250];
	int line;

	std::cout << "Input Image : "; std::cin >> imageInPath; std::cout << std::endl;
	std::cout << "Output Data : "; std::cin >> dataPath; std::cout << std::endl;
	std::cout << "Line index: "; std::cin >> lineChar; std::cout << std::endl; sscanf (lineChar,"%d",&line);

	std::cout << std::endl << imageInPath << " >> " << dataPath << " : " << line << std::endl;

	ImageBase imgIn; imgIn.load(imageInPath);

	int* profil = new int[imgIn.getWidth()];
	std::ofstream flux(dataPath);

	std::cout << "Profil de la ligne " << line << std::endl;
	for(int i = 0; i < imgIn.getWidth();i++)
	{
		profil[i] = imgIn[line][i];
		std::cout << profil[i] << " ";
		flux << i << " " << profil[i] << std::endl;
	}
	std::cout << std::endl;
}

void histoPGM()
{
	std::cout << "   -- Seuillage PGM avec un seul niveau" << std::endl;

	char imageInPath[250] , dataPath[250];
	int line;

	std::cout << "Input Image : "; std::cin >> imageInPath; std::cout << std::endl;
	std::cout << "Output Data : "; std::cin >> dataPath; std::cout << std::endl;

	std::cout << std::endl << imageInPath << " >> " << dataPath << std::endl;

	ImageBase imgIn; imgIn.load(imageInPath);

	int* histo = new int[256];
	for(int i = 0; i < 256;i++){ histo[i] = 0;}

	for(int x = 0; x < imgIn.getHeight(); ++x)
	{
		for(int y = 0; y < imgIn.getWidth(); ++y)
		{
			histo[imgIn[x][y]]++;
		}
	}

	std::ofstream flux(dataPath);

	for(int i = 0; i < 256;i++)
	{
		flux << i << " " << histo[i] << std::endl;
	}
}

void histoPPM()
{
	std::cout << "   -- Seuillage PGM avec un seul niveau" << std::endl;

	char imageInPath[250] , dataPath[250];
	int line;

	std::cout << "Input Image : "; std::cin >> imageInPath; std::cout << std::endl;
	std::cout << "Output Data : "; std::cin >> dataPath; std::cout << std::endl;

	std::cout << std::endl << imageInPath << " >> " << dataPath << std::endl;

	ImageBase imgIn; imgIn.load(imageInPath);

	int* histoR = new int[256];int* histoG = new int[256];int* histoB = new int[256];
	for(int i = 0; i < 256;i++){ histoR[i] = 0;histoG[i] = 0;histoB[i] = 0;}

	for(int x = 0; x < imgIn.getHeight(); ++x)
	{
		for(int y = 0; y < imgIn.getWidth(); ++y)
		{
			histoR[imgIn.get(x,y,0)]++;
			histoG[imgIn.get(x,y,1)]++;
			histoB[imgIn.get(x,y,2)]++;
		}
	}
	
	std::ofstream flux(dataPath);

	for(int i = 0; i < 256;i++)
	{
		flux << i << " " << histoR[i] << " "<< histoG[i] << " "<< histoB[i] << std::endl;
	}
}

void seuillageMultiplePPM()
{
	std::cout << "   -- Seuillage PPM avec plusieurs niveaux" << std::endl;

	char imageInPath[250] , imageOutPath[250], thresholdChar[250];
	int thresholdCount;

	std::cout << "Input Image : "; std::cin >> imageInPath; std::cout << std::endl;
	std::cout << "Output Image : "; std::cin >> imageOutPath; std::cout << std::endl;
	std::cout << "Threshold count : "; std::cin >> thresholdChar; std::cout << std::endl; sscanf (thresholdChar,"%d",&thresholdCount);

	int* thresholds = new int[thresholdCount*3];

	std::cout << "   > Entrez les " << thresholdCount << " niveaux de seuillage pour R : " << std::endl;
	for(int i = 0; i < thresholdCount ; i++)
	{
		std::cin >> thresholdChar;
		sscanf (thresholdChar,"%d",&thresholds[i]);
	}
	std::cout << "   > Entrez les " << thresholdCount << " niveaux de seuillage pour G : " << std::endl;
	for(int i = 0; i < thresholdCount ; i++)
	{
		std::cin >> thresholdChar;
		sscanf (thresholdChar,"%d",&thresholds[thresholdCount + i]);
	}
	std::cout << "   > Entrez les " << thresholdCount << " niveaux de seuillage pour B : " << std::endl;
	for(int i = 0; i < thresholdCount ; i++)
	{
		std::cin >> thresholdChar;
		sscanf (thresholdChar,"%d",&thresholds[thresholdCount + thresholdCount + i]);
	}

	std::cout << std::endl << imageInPath << " >> " << imageOutPath << " : ";
	for(int i = 0; i < thresholdCount ; i++){std::cout << thresholds[i] << " ";}
	std::cout << std::endl;

	ImageBase imgIn; imgIn.load(imageInPath);

	ImageBase imgOut(imgIn.getWidth(), imgIn.getHeight(), true);

	for(int x = 0; x < imgIn.getHeight(); ++x)
	{
		for(int y = 0; y < imgIn.getWidth(); ++y)
		{
			imgOut.set(x,y,0,-1);
			imgOut.set(x,y,1,-1);
			imgOut.set(x,y,2,-1);
		}
	}

	int step = (int)(255.0/thresholdCount);
	for(int x = 0; x < imgIn.getHeight(); ++x)
	{
		for(int y = 0; y < imgIn.getWidth(); ++y)
		{
			for(int rgb = 0 ; rgb < 3; rgb++)
			{
				for(int i = thresholdCount-1 ; i >= 0; i--)
				{
					if(imgIn.get(x,y,rgb) < thresholds[i + (rgb * thresholdCount)])
					{
						imgOut.set(x,y,rgb,step * i);
					}
				}
				if(imgOut.get(x,y,rgb) == -1) { imgOut.set(x,y,rgb,255);}
			}
		}
	}

	std::cout << std::endl << "Saving image" << std::endl;

	imgOut.save(imageOutPath);
}

void seuillageAutoPGM()
{
	std::cout << "   -- Seuillage PGM avec un seul niveau" << std::endl;

	char imageInPath[250] , imageOutPath[250], dataPath[250];

	std::cout << "Input Image : "; std::cin >> imageInPath; std::cout << std::endl;
	std::cout << "Output Image : "; std::cin >> imageOutPath; std::cout << std::endl;
	std::cout << "Output Data : "; std::cin >> dataPath; std::cout << std::endl;

	std::cout << std::endl << imageInPath << " >> " << imageOutPath << std::endl;

	ImageBase imgIn; imgIn.load(imageInPath);

	int* histo = new int[256];
	for(int i = 0; i < 256;i++){ histo[i] = 0;}

	for(int x = 0; x < imgIn.getHeight(); ++x)
	{
		for(int y = 0; y < imgIn.getWidth(); ++y)
		{
			histo[imgIn[x][y]]++;
		}
	}

	double* pentes = new double[256];
	for(int i = 0; i < 256;i++){ pentes[i] = -1;}

	int distance = 6;

	for(int i = distance ; i < 256 - distance ; i++)
	{
		double p = 0;
		for(int d = -distance; d <= distance ; d++)
		{
			if(d <= 0)
				p += histo[i] - histo[i+d];
			else
				p -= histo[i] - histo[i+d];
		}
		pentes[i] = p / (distance*2.0);
	}

	std::ofstream flux(dataPath);
	for(int i = 0; i < 256;i++)
	{
		flux << i << " " << pentes[i] << std::endl;
	}

	int maxUp = 0, maxDown = 0;
	for(int i = 0; i < 128-distance; i++)
	{
		if(abs(pentes[128+i]) >= abs(pentes[maxUp]))
		{
			maxUp = 128+i;
		}
		if(abs(pentes[128-i]) >= abs(pentes[maxDown]))
		{
			maxDown = 128-i;
		}
	}

	std::cout << "MaxUp = " << maxUp << "  MaxDown = " << maxDown << std::endl;

	int threshold = (int)((maxUp + maxDown) / 2.0);

	ImageBase imgOut(imgIn.getWidth(), imgIn.getHeight(), false);

	for(int x = 0; x < imgIn.getHeight(); ++x)
	{
		for(int y = 0; y < imgIn.getWidth(); ++y)
		{
			if (imgIn[x][y] < threshold)
			{
				imgOut[x][y] = 0;
			}
			else
			{
				imgOut[x][y] = 255;
			}
		}
	}

	std::cout << std::endl << "Saving image" << std::endl;

	imgOut.save(imageOutPath);
}


int main(int argc, char **argv)
{
	int question;
  
	if (argc != 2) 
	{
		printf("Usage: Question \n   > 0 : seuillage pgm \n   > 1 : seuillage multiple pgm \n"); 
		return 1;
	}
	sscanf (argv[1],"%d",&question) ;

	std::cout << std::endl;

	switch(question)
	{
		default:return 0;
		case 0:seuillagePGM();break;
		case 1:seuillagePGM();break;
		case 2:seuillageMultiplePGM();break;
		case 3:profilPGM();break;
		case 4:histoPGM();break;
		case 5:histoPPM();break;
		case 6:seuillageMultiplePPM();break;
		case 7:seuillageAutoPGM();break;
	}


	return 0;
}

/*
///////////////////////////////////////// Exemple d'un seuillage d'image
	char cNomImgLue[250], cNomImgEcrite[250];
	int S;
  
	if (argc != 4) 
	{
		printf("Usage: ImageIn.pgm ImageOut.pgm Seuil \n"); 
		return 1;
	}
	sscanf (argv[1],"%s",cNomImgLue) ;
	sscanf (argv[2],"%s",cNomImgEcrite);
	sscanf (argv[3],"%d",&S);
	
	
	//ImageBase imIn, imOut;
	ImageBase imIn;
	imIn.load(cNomImgLue);

	//ImageBase imG(imIn.getWidth(), imIn.getHeight(), imIn.getColor());
	ImageBase imOut(imIn.getWidth(), imIn.getHeight(), imIn.getColor());

	for(int x = 0; x < imIn.getHeight(); ++x)
		for(int y = 0; y < imIn.getWidth(); ++y)
		{
			if (imIn[x][y] < S) 
				imOut[x][y] = 0;
			else imOut[x][y] = 255;
		}
		
	imOut.save(cNomImgEcrite);
		

	
	
	///////////////////////////////////////// Exemple de cr�ation d'une image couleur
	ImageBase imC(50, 100, true);

	for(int y = 0; y < imC.getHeight(); ++y)
		for(int x = 0; x < imC.getWidth(); ++x)
		{
			imC[y*3][x*3+0] = 200; // R
			imC[y*3][x*3+1] = 0; // G
			imC[y*3][x*3+2] = 0; // B
		}
		
	imC.save("imC.ppm");
		



	///////////////////////////////////////// Exemple de cr�ation d'une image en niveau de gris
	ImageBase imG(50, 100, false);

	for(int y = 0; y < imG.getHeight(); ++y)
		for(int x = 0; x < imG.getWidth(); ++x)
			imG[y][x] = 50;

	imG.save("imG.pgm");




	ImageBase imC2, imG2;
	
	///////////////////////////////////////// Exemple lecture image couleur
	imC2.load("imC.ppm");
	///////////////////////////////////////// Exemple lecture image en niveau de gris
	imG2.load("imG.pgm");
	
	

	///////////////////////////////////////// Exemple de r�cup�ration d'un plan de l'image
	ImageBase *R = imC2.getPlan(ImageBase::PLAN_R);
	R->save("R.pgm");
	delete R;
	*/

#include "../Library/ImageBase.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <cmath>
#include <math.h>

using namespace std;

double paletteDistance(vector<Color> p)
{
	double mV = 0; int mC = 0;
	for(int a = 0; a < p.size(); a++)
	{
		for(int b = 0; b < p.size() ; b++)
		{
			if(a==b){continue;}
			mV += p[a].distance(p[b]);
			mC++;
		}
	}
	mV /= mC;
	return mV;
}

void displayPalette(vector<Color> palette)
{
	cout << "Palette [" << paletteDistance(palette) << "]:" << endl;
	for(int i = 0; i < palette.size(); i++)
	{
		cout << "  - " << (int)palette.at(i).r << " " << (int)palette.at(i).g << " " << (int)palette.at(i).b;

		if(i % 6 == 0) {cout << endl;}
		else{ cout << "  |   ";}
	}
	cout << endl;
}

vector<Color> pickColors(ImageBase& img, int count, double minDistance, int maxTry)
{
	vector<Color> pal = vector<Color>(); pal.resize(count);
	double dist = 0;

	for(int i = 0; i < maxTry;i++)
	{
		vector<Color> palTemp = vector<Color>(); palTemp.resize(count);
		//Picking 3 random pixels
		for(int c = 0; c < count; c++)
		{
			int id = (int)((std::rand()/(float)RAND_MAX) * img.getSize()) ;
			palTemp[c] = img.readColor(id);
		}
		//Calculating average distance
		double mV = paletteDistance(palTemp);
		//Checking distance
		if(mV >= minDistance)
		{
			pal = palTemp; break;
		}
		else if(mV > dist)
		{
			pal = palTemp;
		}
	}

	return pal;
}

void classify(ImageBase& imgIn,ImageBase& imgOut, vector<Color> palette)
{
	for(int i = 0; i < imgIn.getSize(); i++)
	{
		int pId = 0; double pD = 1000;
		for(int p = 0; p < palette.size(); p++)
		{
			double d = imgIn.readColor(i).distance(palette.at(p));
			if(d < pD)
			{
				pId = p; pD = d;
			}
		}

		imgOut.setColor(i,palette.at(pId));
	}
}

vector<Color>  classifyAverageRecursive(ImageBase& imgIn, ImageBase& imgOut, vector<Color> palette, int iteration, int maxIteration, double targetDistance)
{
	vector<int> classes[palette.size()];

	for(int i = 0; i < imgIn.getSize(); i++)
	{
		int pId = 0; double pD = 1000;
		for(int p = 0; p < palette.size(); p++)
		{
			double d = imgIn.readColor(i).distance(palette.at(p));
			if(d < pD)
			{
				pId = p; pD = d;
			}
		}

		classes[pId].push_back(i);
	}

	vector<Color> classeColors; classeColors.resize(palette.size());

	for(int i = 0; i < palette.size(); i++)
	{
		if(classes[i].size() <= 0){continue;}

		int r = 0; int g = 0; int b = 0;

		for(int c = 0; c < classes[i].size(); c++)
		{
			r += imgIn.readColor(classes[i].at(c)).r;
			g += imgIn.readColor(classes[i].at(c)).g;
			b += imgIn.readColor(classes[i].at(c)).b;
		}

		r /= classes[i].size();
		g /= classes[i].size();
		b /= classes[i].size();

		classeColors[i] = Color(r,g,b);
	}

	double distance = 0;
	for(int i = 0; i < palette.size(); i++)
	{
		distance +=
			abs(classeColors.at(i).r - palette.at(i).r) +
			abs(classeColors.at(i).g - palette.at(i).g) +
			abs(classeColors.at(i).b - palette.at(i).b);
	}
	distance /= palette.size();

	cout << " > Iteration " << iteration << " change = " << distance << " | " << endl;
	//displayPalette(classeColors);

	if(distance <= targetDistance || iteration >= maxIteration)
	{
		for(int i = 0; i < classeColors.size(); i++)
		{
			for(int c = 0; c < classes[i].size(); c++)
			{
				if(imgOut.getColor())
				{
					imgOut.setColor(classes[i].at(c), classeColors.at(i));
				}
				else
				{
					imgOut.set(classes[i].at(c),0,i);
				}
			}
		}
		return classeColors;
	}

	return classifyAverageRecursive(imgIn,imgOut,classeColors,iteration+1,maxIteration,targetDistance);
}

vector<Color>  classifyAverage(ImageBase& imgIn,ImageBase& imgOut, vector<Color> palette, int maxIteration, double targetDistance)
{
	 return classifyAverageRecursive(imgIn,imgOut,palette,0,maxIteration,targetDistance);
}

double EQM(ImageBase& imgIn,ImageBase& imgOut)
{
	double r = 0; double g = 0; double b = 0;

	for(int i = 0; i < imgIn.getSize(); i++)
	{
		Color cIn = imgIn.readColor(i);
		Color cOut = imgOut.readColor(i);

		r += (cIn.r - cOut.r) * (cIn.r - cOut.r) ;
		g += (cIn.g - cOut.g) * (cIn.g - cOut.g) ;
		b += (cIn.b - cOut.b) * (cIn.b - cOut.b) ;
	}

	int tIn = imgIn.getSize() ;

	r /= tIn;
	g /= tIn;
	b /= tIn;

	return (r+g+b) / 3.0;
}

double PSNR(ImageBase& imgIn, ImageBase& imgOut)
{
	return 10 * log10((255*255) / EQM(imgIn,imgOut));
}

void savePalette(vector<Color> palette)
{
	ImageBase imgOut(16,16, true);

	for(int i = 0; i < 256; i++){imgOut.setColor(i,palette.at(i));}

	imgOut.save("palette.ppm");
}

int main(int argc, char **argv)
{
	srand(std::time(nullptr));

	if (argc != 4)
	{
		printf("Usage: Image In ; Image out ; Palette size");
		return 1;
	}
	char imageInPath[250] , imageOutPath[250];int pSize;
	sscanf (argv[1],"%s",imageInPath) ;
	sscanf (argv[2],"%s",imageOutPath) ;
	sscanf (argv[3],"%d",&pSize) ;

	cout << "Loading image " << imageInPath << endl;

	ImageBase image; image.load(imageInPath);
	cout << " > Size : " << image.getSize() << endl;

	vector<Color> palette;

	int autoPalette;
	cout << "Auto-generate palette ? (0 : 1) : ";
	scanf("%i",&autoPalette);

	if(autoPalette == 1)
	{
		int pDistance;
		cout << "Minimum color-distance : ";
		scanf ("%i",&pDistance) ;

		cout << " > Processing palette with " << pSize << " colors" <<endl;

		palette = pickColors(image,pSize,pDistance, 1000);
	}
	else
	{
		palette = vector<Color>();
		for(int i = 0 ; i < pSize; i++)
		{
			int r,g,b;
			cout << "Enter color values (\"r g b\") : ";
			scanf("%i %i %i",&r, &g, &b);

			palette.push_back(Color(r,g,b));
		}
	}

	cout << " >";
	displayPalette(palette);

	int classifyType;
	cout << "Classifying method (0 : palette , 1 : average colors , 2 : average classes) : ";
	scanf("%i",&classifyType);

	cout << "Classifying image" << endl;

	ImageBase imgOut(image.getWidth(), image.getHeight(), (classifyType == 0) || classifyType == 1);

	if(classifyType == 0)
	{
		classify(image,imgOut,palette);
	}
	else
	{
		vector<Color> finalPalette = classifyAverage(image,imgOut,palette,100,2);

		cout << "Done" << endl;

		if(classifyType == 2)
		{
			displayPalette(finalPalette);
			savePalette(finalPalette);
		}
	}

	double psnr = PSNR(image,imgOut);
	cout << " > PSNR : " << psnr << endl;

	cout  << "Saving image" << endl;

	imgOut.save(imageOutPath);

	return 0;
}

#include "../Library/ImageBase.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

ImageBase* getGrey(ImageBase* image)
{
	ImageBase* img = new ImageBase(image->getWidth(), image->getHeight(),false);

	for(int i = 0; i < image->getSize();i++)
	{
		img->set(i,0,image->readColor(i).getY());
	}

	return img;
}

struct Region
{
	int xmin,xmax,ymin,ymax,deltaX,deltaY,size;
	float moyenne,variance;

	Region* right; Region* left; Region* up; Region* down; bool merged;

	void process(ImageBase* image)
	{
		moyenne = 0;
		for(int y = ymin; y < ymax; y++)
		{
			for(int x = xmin; x < xmax;x++)
			{
				moyenne += image->get(x,y,0); 
			}
		}
		moyenne /= size;

		variance = 0;
		for(int y = ymin; y < ymax; y++)
		{
			for(int x = xmin; x < xmax;x++)
			{
				float v = (image->get(x,y,0) - moyenne);
				variance += v*v; 
			}
		}
		variance /= size;
	}

	Region(float _m,int _xmin, int _xmax, int _ymin, int _ymax)
	{
		moyenne = _m;
		xmax = _xmax; xmin = _xmin; ymax = _ymax; ymin = _ymin;
		deltaX = xmax - xmin;
		deltaY = ymax - ymin;
		size = deltaX * deltaY;
		left = nullptr; right = nullptr; up = nullptr; down = nullptr;
	}

	Region(ImageBase* image,int _xmin, int _xmax, int _ymin, int _ymax)
	{
		xmax = _xmax; xmin = _xmin; ymax = _ymax; ymin = _ymin;
		deltaX = xmax - xmin;
		deltaY = ymax - ymin;
		size = deltaX * deltaY;
		left = nullptr; right = nullptr; up = nullptr; down = nullptr;
		process(image);
	}
};

void draw(vector<Region*> regions, ImageBase* imgOut)
{
	for(int i = 0; i < regions.size();i++)
	{
		for(int y = regions.at(i)->ymin; y < regions.at(i)->ymax; y++)
		{
			for(int x = regions.at(i)->xmin; x < regions.at(i)->xmax;x++)
			{
				imgOut->set(x,y,0,regions.at(i)->moyenne);
			}
		}
	}
}

vector<Region*> divide4(ImageBase* image)
{
	vector<Region*> regions;

	regions.push_back(new Region(image,0,image->getWidth()/2,0,image->getHeight()/2));
	regions.push_back(new Region(image,image->getWidth()/2,image->getWidth(),0,image->getHeight()/2));
	regions.push_back(new Region(image,0,image->getWidth()/2,image->getHeight()/2,image->getHeight()));
	regions.push_back(new Region(image,image->getWidth()/2,image->getWidth(),image->getHeight()/2,image->getHeight()));

	return regions;
}

void divideRecursive(ImageBase* image,Region* region,vector<Region*>& regions,int depth,float targetVariance)
{
	/*for(int i = 0; i < 4 - depth; i++){cout << "  ";}
	cout << region->xmin << " " << region->xmax <<  " " << region->ymin << " " << region->ymax << endl;*/

	if(depth <= 0 || region->variance <= targetVariance || region->deltaX <= 4 || region->deltaY <= 4) 
	{
		regions.push_back(region);
		return;
	}

	Region* a = new Region(image,region->xmin,region->xmin + region->deltaX/2,region->ymin,region->ymin + region->deltaY/2);
	Region* b = new Region(image,region->xmin + region->deltaX/2,region->xmin + region->deltaX,region->ymin,region->ymin + region->deltaY/2);
	Region* c = new Region(image,region->xmin,region->xmin + region->deltaX/2,region->ymin + region->deltaY/2,region->ymin + region->deltaY);
	Region* d = new Region(image,region->xmin + region->deltaX/2,region->xmin + region->deltaX,region->ymin + region->deltaY/2,region->ymin + region->deltaY);

	divideRecursive(image,a,regions,depth-1,targetVariance);
	divideRecursive(image,b,regions,depth-1,targetVariance);
	divideRecursive(image,c,regions,depth-1,targetVariance);
	divideRecursive(image,d,regions,depth-1,targetVariance);
}

vector<Region*> divide(ImageBase* image, int depth,float targetVariance)
{
	vector<Region*> baseRegions = divide4(image);
	vector<Region*> regions;

	for(int i = 0; i < 4; i++)
	{
		cout << "Variance : " << baseRegions.at(i)->variance << endl;
		divideRecursive(image,baseRegions.at(i),regions,depth,targetVariance);
	}

	return regions;
}

void buildGraph(vector<Region*> regions)
{
	for(int i = 0; i < regions.size();i++)
	{
		Region* regionA = regions.at(i);
		for(int j = 0; j < regions.size(); j++)
		{
			if(i == j){continue;}

			Region* regionB = regions.at(j);

			if(	(regionA->xmin >= regionB->xmin && regionA->xmin <=regionB->xmax) ||
				(regionA->xmax >= regionB->xmin && regionA->xmax <=regionB->xmax) )
			{
				if(regionA->ymin == regionB->ymax) 
				{
					regionA->down = regionB;
					regionB->up = regionA;
				}
				else if(regionA->ymax == regionB->ymin)
				{
					regionA->down = regionB;
					regionB->up = regionA;
				} 
			}
			else if(	(regionA->ymin >= regionB->ymin && regionA->ymin <= regionB->ymax) ||
						(regionA->ymax >= regionB->ymin && regionA->ymax <= regionB->ymax) )
			{
				if(regionA->xmin == regionB->xmax) 
				{
					regionA->right = regionB;
					regionB->left = regionA;
				}
				else if(regionA->xmax == regionB->xmin)
				{
					regionA->left = regionB;
					regionB->right = regionA;
				} 
			}
		}
	}
}

vector<Region*> regroup(vector<Region*> regions, float targetMoyenneDiff)
{
	vector<Region*> outRegions;

	for(int i = 0; i < regions.size();i++)
	{
		if(regions.at(i)->merged){continue;}

		if(regions.at(i)->right != nullptr)
		{
			if( abs(regions.at(i)->moyenne - regions.at(i)->right->moyenne) <= targetMoyenneDiff)// && !regions.at(i)->right->merged)
			{
				Region* aRegion = new Region( (regions.at(i)->moyenne + regions.at(i)->right->moyenne) / 2,regions.at(i)->xmin,regions.at(i)->xmax,regions.at(i)->ymin,regions.at(i)->ymax);
				Region* bRegion = new Region( (regions.at(i)->moyenne + regions.at(i)->right->moyenne) / 2,regions.at(i)->right->xmin,regions.at(i)->right->xmax,regions.at(i)->right->ymin,regions.at(i)->right->ymax);
				outRegions.push_back(aRegion); outRegions.push_back(bRegion);
				regions.at(i)->merged = true; regions.at(i)->right->merged = true;
			}
		}
		else if(regions.at(i)->left != nullptr)
		{
			if( abs(regions.at(i)->moyenne - regions.at(i)->left->moyenne) <= targetMoyenneDiff)// && !regions.at(i)->left->merged)
			{
				Region* aRegion = new Region( (regions.at(i)->moyenne + regions.at(i)->left->moyenne) / 2,regions.at(i)->xmin,regions.at(i)->xmax,regions.at(i)->ymin,regions.at(i)->ymax);
				Region* bRegion = new Region( (regions.at(i)->moyenne + regions.at(i)->left->moyenne) / 2,regions.at(i)->left->xmin,regions.at(i)->left->xmax,regions.at(i)->left->ymin,regions.at(i)->left->ymax);
				outRegions.push_back(aRegion); outRegions.push_back(bRegion);
				regions.at(i)->merged = true;regions.at(i)->left->merged = true;
			}
		}
		else if(regions.at(i)->up != nullptr)
		{
			if( abs(regions.at(i)->moyenne - regions.at(i)->up->moyenne) <= targetMoyenneDiff)// && !regions.at(i)->up->merged)
			{
				Region* aRegion = new Region( (regions.at(i)->moyenne + regions.at(i)->up->moyenne) / 2,regions.at(i)->xmin,regions.at(i)->xmax,regions.at(i)->ymin,regions.at(i)->ymax);
				Region* bRegion = new Region( (regions.at(i)->moyenne + regions.at(i)->up->moyenne) / 2,regions.at(i)->up->xmin,regions.at(i)->up->xmax,regions.at(i)->up->ymin,regions.at(i)->up->ymax);
				outRegions.push_back(aRegion); outRegions.push_back(bRegion);
				regions.at(i)->merged = true;regions.at(i)->up->merged = true;
			}
		}
		else if(regions.at(i)->down != nullptr)
		{
			if( abs(regions.at(i)->moyenne - regions.at(i)->down->moyenne) <= targetMoyenneDiff)// && !regions.at(i)->down->merged )
			{
				Region* aRegion = new Region( (regions.at(i)->moyenne + regions.at(i)->down->moyenne) / 2,regions.at(i)->xmin,regions.at(i)->xmax,regions.at(i)->ymin,regions.at(i)->ymax);
				Region* bRegion = new Region( (regions.at(i)->moyenne + regions.at(i)->down->moyenne) / 2,regions.at(i)->down->xmin,regions.at(i)->down->xmax,regions.at(i)->down->ymin,regions.at(i)->down->ymax);
				outRegions.push_back(aRegion); outRegions.push_back(bRegion);
				regions.at(i)->merged = true;regions.at(i)->down->merged = true;
			}
		}
		else 
		{
			outRegions.push_back(regions.at(i));
		}
	}

	return outRegions;
}

int main(int argc, char **argv)
{
	if (argc != 3)
	{
		printf("Usage: ImageIn ; ImageOut ;\n");
		return 1;
	}
	char imageInPath[250] , imageOutPath[250];
	sscanf (argv[1],"%s",imageInPath) ;
	sscanf (argv[2],"%s",imageOutPath) ;

	cout << endl;

	cout << "Loading image " << imageInPath << endl;

	ImageBase* image = new ImageBase(); image->load(imageInPath);
	cout << " > Size : " << image->getSize() << " | " << (image->getColor() ? "PPM" : "PGM") << endl;
	
	ImageBase* imgOut = new ImageBase(image->getWidth(), image->getHeight(),false);

	cout << "Processing regions" << endl;
	vector<Region*> regions = divide(image,10,100);

	cout << "Drawing regions" << endl;
	draw(regions,imgOut);
	cout << "Saving regions image" << endl;
	imgOut->save("regions.pgm");

	cout << "Building graph" << endl;
	buildGraph(regions);
	cout << "Regrouping" << endl;
	regions = regroup(regions,1);
	cout << "Drawing regions" << endl;
	draw(regions,imgOut);


	cout << "Saving merged region image" << endl;
	imgOut->save("merged.pgm");

	return 0;
}

/*
///////////////////////////////////////// Exemple d'un seuillage d'image
	char cNomImgLue[250], cNomImgEcrite[250];
	int S;
  
	if (argc != 4) 
	{
		printf("Usage: ImageIn.pgm ImageOut.pgm Seuil \n"); 
		return 1;
	}
	sscanf (argv[1],"%s",cNomImgLue) ;
	sscanf (argv[2],"%s",cNomImgEcrite);
	sscanf (argv[3],"%d",&S);
	
	
	//ImageBase imIn, imOut;
	ImageBase imIn;
	imIn.load(cNomImgLue);

	//ImageBase imG(imIn.getWidth(), imIn.getHeight(), imIn.getColor());
	ImageBase imOut(imIn.getWidth(), imIn.getHeight(), imIn.getColor());

	for(int x = 0; x < imIn.getHeight(); ++x)
		for(int y = 0; y < imIn.getWidth(); ++y)
		{
			if (imIn[x][y] < S) 
				imOut[x][y] = 0;
			else imOut[x][y] = 255;
		}
		
	imOut.save(cNomImgEcrite);
		

	
	
	///////////////////////////////////////// Exemple de cr�ation d'une image couleur
	ImageBase imC(50, 100, true);

	for(int y = 0; y < imC.getHeight(); ++y)
		for(int x = 0; x < imC.getWidth(); ++x)
		{
			imC[y*3][x*3+0] = 200; // R
			imC[y*3][x*3+1] = 0; // G
			imC[y*3][x*3+2] = 0; // B
		}
		
	imC.save("imC.ppm");
		



	///////////////////////////////////////// Exemple de cr�ation d'une image en niveau de gris
	ImageBase imG(50, 100, false);

	for(int y = 0; y < imG.getHeight(); ++y)
		for(int x = 0; x < imG.getWidth(); ++x)
			imG[y][x] = 50;

	imG.save("imG.pgm");




	ImageBase imC2, imG2;
	
	///////////////////////////////////////// Exemple lecture image couleur
	imC2.load("imC.ppm");
	///////////////////////////////////////// Exemple lecture image en niveau de gris
	imG2.load("imG.pgm");
	
	

	///////////////////////////////////////// Exemple de r�cup�ration d'un plan de l'image
	ImageBase *R = imC2.getPlan(ImageBase::PLAN_R);
	R->save("R.pgm");
	delete R;
	*/

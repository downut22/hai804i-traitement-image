#include "../Library/ImageBase.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <math.h>
#include <climits>

using namespace std;

int sizeX,sizeY,sizeZ,sizer;
unsigned short* datar;

unsigned short getValue(int i, int j , int k)
{
	return datar[i + (j*sizeX) + (k*sizeX*sizeY)];
}

unsigned short getMaxValue(unsigned short* array,int arraysize)
{
	unsigned short v = 0;

	for(int i = 0; i < arraysize; i++)
	{
		v = max(v,array[i]);
	}

	return v;
}

unsigned short getMinValue(unsigned short* array,int arraysize)
{
	unsigned short v = USHRT_MAX;

	for(int i = 0; i < arraysize; i++)
	{
		v = min(v,array[i]);
	}

	return v;
}

unsigned short project(int x,int y, int axis,int mode)
{
	int value = mode == 2 ? INT_MAX : 0;

	int sized = axis == 0 ? sizeX : axis == 1 ? sizeY : sizeZ;
	
	for(int i = 0; i < sized; i++)
	{
		int v =  axis == 0 ? getValue(i,x,y) : axis == 1 ? getValue(x,i,y) : getValue(x,y,i);
		value = mode == 0 ? max(value,v) : mode == 1 ? value + v : min(value,v) ;
	}

	if(mode == 1)
	{
		value /= sized;
	}

	return (unsigned short)value;
}

void render(unsigned short*& img,int& size1, int& size2,int axis, int mode)
{
	size1 = axis == 0 ? sizeY : axis == 1 ? sizeX : sizeX; 
	size2 = axis == 0 ? sizeZ : axis == 1 ? sizeZ : sizeY;

	int img_size = size1*size2;
	img = new unsigned short[img_size];

	for(int x = 0; x < size1; x++)
	{
		for(int y = 0; y < size2; y++)
		{
			unsigned short value = project(x,y,axis,mode);
			img[x + y*size1] = value;
		}
	}
}

ImageBase* toPGM(unsigned short* img_dat,int size1, int size2)
{
	ImageBase* img = new ImageBase(size1,size2,false);

	unsigned short maxV = getMaxValue(img_dat,size1*size2);
	unsigned short minV = getMinValue(img_dat,size1*size2);

	cout <<  "Max value : " << maxV << "  Min value : " << minV << endl;

	for(int i = 0; i < size1*size2; i++)
	{
		double v = (img_dat[i] - minV) / (double)(maxV-minV);
		img->set(i,0,(int)(v*255));
	}

	return img;
}

int main(int argc, char **argv)
{
	if (argc != 8)
	{
		printf("Usage: ImageIn ; X ; Y ; Z ; ImageOutPGM ; axis ; mode\n");
		return 1;
	}
	char imageInPath[250],imageOutPath[250]; int axis,mode;
	sscanf (argv[1],"%s",imageInPath) ;
	sscanf (argv[2],"%d",&sizeX) ;
	sscanf (argv[3],"%d",&sizeY) ;
	sscanf (argv[4],"%d",&sizeZ) ;
	sscanf (argv[5],"%s",&imageOutPath) ;
	sscanf (argv[6],"%d",&axis) ;
	sscanf (argv[7],"%d",&mode) ;

	cout << endl << "Opening " << imageInPath << " of size X = " << sizeX << " Y = " << sizeY << " Z = " << sizeZ << endl;

	sizer = sizeX*sizeY*sizeZ;
	datar = new unsigned short[sizer];

	unsigned char* datac = new unsigned char[sizer*2];

	FILE* file = fopen(imageInPath,"rb");
	int readed = fread(datac,sizeof(unsigned char),sizer*2,file);

	cout << "Successfully read " << readed << " char values" << endl;

	for(int i = 0; i < sizer*2; i+=2)
	{
		datar[i/2] = (unsigned short)datac[i+1] + ((unsigned short)datac[i]*256);
	}

	unsigned short maxValue = getMaxValue(datar,sizer);
	unsigned short minValue = getMinValue(datar,sizer);

	cout << "Max value " << maxValue << "   Min value " << minValue << endl;

	/*int x,y,z;
	scanf("%d %d %d",&x,&y,&z);

	cout << "Reading value at " << x << " " << y << " " << z << endl;

	unsigned short value = getValue(x,y,z);

	cout << "Value > "<< value << endl;*/

	cout << "Rendering through axis " << (axis == 0 ? "x" : axis == 1 ? "y" : "z") << " with mode " << (mode == 0 ? "MIP" : mode == 1 ? "AIP" : "MiniP") << endl;

	unsigned short* img; int size1,size2;
	render(img,size1,size2,axis,mode);

	ImageBase* imgOut = toPGM(img,size1,size2);
	imgOut->save(imageOutPath);

	return 0;
}
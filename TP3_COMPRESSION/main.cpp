#include "../Library/ImageBase.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>
#include <cmath>

using namespace std;

int* histoPGM(ImageBase* image)
{
	int* histo = new int[256];
	for(int i = 0; i < 256;i++){ histo[i] = 0;}

	for(int x = 0; x < image->getHeight(); ++x)
	{
		for(int y = 0; y < image->getWidth(); ++y)
		{
			histo[image->get(x,y,0)]++;
		}
	}

	return histo;
}


int predict(int a, int b, int c)
{
	/*if(c >= max(a,b)){return min(a,b);}
	else if(c <= min(a,b)){return max(a,b);}
	else{return a + b - c;}*/
	return a+c-b;//(int)((3*(a+c) - 2*b)/4.0);
}

void differenceMap(ImageBase* image, ImageBase* map)
{
	int c = 0;
	for(int x = 0; x < image->getWidth(); x++)
	{
		for(int y = 0; y < image->getHeight(); y++)
		{
			int a = (x > 0) ? image->get(x-1,y,0) : image->get(x,y,0);
			int b = (x > 0 && y > 0) ? image->get(x-1,y-1,0) : image->get(x,y,0);
			int c = (y > 0) ? image->get(x,y-1,0) : image->get(x,y,0);

			int diff = (predict(a,b,c) -  image->get(x,y,0)) + 128;
			if(diff < 0 || diff > 255){c++;}
			diff = max(0,min(255,diff));

			map->set(x,y,0,diff);
		}
	}

	cout << "Débordements : " << c << endl;
}

double getEntropy(ImageBase* image)
{
	int* histo = histoPGM(image);

	double e = 0;
	for(int i = 0; i < 256;i++)
	{
		double v = histo[i] / (double)image->getSize();
		if(v <= 0){continue;}
		e += (-1) * (v * log2(v));
	}

	return e;
}

void saveHisto(int* histo, char* path)
{
	std::ofstream flux(path);
	for(int i = 0; i < 256;i++)	{	flux << i << " " << histo[i] << std::endl;}
}

void reconstruct(ImageBase* map, ImageBase* image)
{
	image->set(0,0,0,map->get(0,0,0));

	for(int x = 0; x < map->getWidth(); x++)
	{
		for(int y = 0; y < map->getHeight(); y++)
		{
			int a = (x > 0) ? image->get(x-1,y,0) : image->get(x,y,0);
			int b = (x > 0 && y > 0) ? image->get(x-1,y-1,0) : image->get(x,y,0);
			int c = (y > 0) ? image->get(x,y-1,0) : image->get(x,y,0);

			int v = predict(a,b,c) - ((int)map->get(x,y,0) - 128);
			if(v < 0 || v > 255) cout << v << endl;
			//v = min(max(v,0),255);
			image->set(x,y,0,v);
		}
	}
}

int main(int argc, char **argv)
{
	srand(std::time(nullptr));

	if (argc != 4)
	{
		printf("Usage: Image In ; Map ; Image out ");
		return 1;
	}
	char imageInPath[250] , mapPath[250], imageOutPath[250];
	sscanf (argv[1],"%s",imageInPath) ;
	sscanf (argv[2],"%s",mapPath) ;
	sscanf (argv[3],"%s",imageOutPath) ;

	ImageBase* image = new ImageBase(); image->load(imageInPath);

	int* histo = histoPGM(image);	
	saveHisto(histo,"histoImage.dat");

	double entropy = getEntropy(image);
	cout << "Entropy : " << entropy  << " | image size : " << image->getSize() << endl;

	ImageBase* map = new ImageBase(image->getWidth(), image->getHeight(), false);

	cout << "Processing difference map..." << endl;

	differenceMap(image,map);

	histo = histoPGM(map);	
	saveHisto(histo,"histoMap.dat");

	cout << "Saving MAP" << endl;

	map->save(mapPath);

	ImageBase* imgOut = new ImageBase(map->getWidth(), map->getHeight(),false);

	reconstruct(map,imgOut);

	imgOut->save("reconstruct.pgm");
	
	cout << "END" << endl;

	return 0;
}

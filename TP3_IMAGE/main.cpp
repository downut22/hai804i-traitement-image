#include "../Library/ImageBase.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>

using namespace std;

unsigned char expandValue(int pixel, int a0, int a1)
{
	return (unsigned char)max(0.0,min(255.0,(-255/(double)(a1-a0)) * (a0 - pixel)));
}

void expand(ImageBase* image, ImageBase* imgOut)
{
	int* histo = image->histogram(0);

	int amin = 0; int amax = 255;

	int a0 = -1; int a1 = -1; 
	/*int count = 0; double target0 = image->getSize() * ratio; double target1 = image->getSize()-target0;

	for(int i = 0; i < 256; i++)
	{
		count += histo[i];

		if(a0 < 0 && count >= target0){a0 = i;}
		if(a1 < 0 && count >= target1){a1 = i;}
	}*/
	for(int i = 0; i < 256; i++)
	{
		if(a0 < 0 && histo[i] > 0){a0=i;}
		if(a1 < 0 && histo[255-i] > 0){a1 = 255-i;}
	}


	int alpha = (amin*a1 - amax*a0) / (a1 - a0);
	int beta = (amax-a0 - amin*a1) / (a1 - a0);

	cout << "Expanding with <a0 : " << a0 << "> and <a1 : " << a1 << " , <alpha : " << alpha << "> and <beta : " << beta << ">" << endl; 

	for(int i = 0; i < image->getSize();i++)
	{
		imgOut->set(i,0,expandValue(image->get(i,0),a0,a1));
	}
}

void seuillage(ImageBase* image, ImageBase* imgOut, int smin, int smax)
{
	cout << "Seuillage entre " << smin << " et " << smax << endl;
	for(int i = 0; i < image->getSize();i++)
	{
		int v = image->get(i,0);

		if(v <= smin){ imgOut->set(i,0,smin); }
		else if(v >= smax){ imgOut->set(i,0,smax); }
		else{imgOut->set(i,0,v);}
	}
}

void egalize(ImageBase* image, ImageBase* imgOut)
{
	double* rep = image->repartition(0);
	for(int i = 0; i < image->getSize();i++)
	{
		unsigned char v = (unsigned char)max(0.0,min(255.0,(255 * rep[image->get(i,0)])));
		imgOut->set(i,0,v);
	}
}

void specify(ImageBase* image, ImageBase* reference,ImageBase* imgOut)
{
	double* rep = reference->repartition(0);

	for(int i = 0; i < image->getSize();i++)
	{
		unsigned char v = image->get(i,0);

		int index = 0; float d = 1000;
		for(int i = 0; i < 256; i++)
		{
			float d2 = abs( (rep[i]*255) - v);
			if(d2 < d)
			{
				index = i;
				d = d2;
			}
		}

		imgOut->set(i,0,index);
	}
}

void mainExpansion(ImageBase* image, ImageBase* imgOut)
{

	if(!image->getColor()) {
		expand(image,imgOut);
	}
	else
	{
		ImageBase* R = image->getPlan(ImageBase::PLAN_R);
		ImageBase* G = image->getPlan(ImageBase::PLAN_G);
		ImageBase* B = image->getPlan(ImageBase::PLAN_B);

		ImageBase* imgOutR = new ImageBase(image->getWidth(), image->getHeight(),false);
		ImageBase* imgOutG = new ImageBase(image->getWidth(), image->getHeight(),false);
		ImageBase* imgOutB = new ImageBase(image->getWidth(), image->getHeight(),false);

		expand(R,imgOutR);
		expand(G,imgOutG);
		expand(B,imgOutB);

		for(int i = 0; i < imgOutR->getSize();i++)
		{
			imgOut->set(i,0,imgOutR->get(i,0));
			imgOut->set(i,1,imgOutG->get(i,1));
			imgOut->set(i,2,imgOutB->get(i,2));
		}
	}
}

void mainSeuillage(ImageBase* image, ImageBase* imgOut)
{
	ImageBase* R = image->getPlan(ImageBase::PLAN_R);
	ImageBase* G = image->getPlan(ImageBase::PLAN_G);
	ImageBase* B = image->getPlan(ImageBase::PLAN_B);

	ImageBase* imgOutR = new ImageBase(image->getWidth(), image->getHeight(),false);
	ImageBase* imgOutG = new ImageBase(image->getWidth(), image->getHeight(),false);
	ImageBase* imgOutB = new ImageBase(image->getWidth(), image->getHeight(),false);

	cout << "Seuils R max min : "; int smin,smax;
	scanf("%i %i",&smin,&smax);
	seuillage(R,imgOutR,smin,smax);

	cout << "Seuils G max min : "; 
	scanf("%i %i",&smin,&smax);
	seuillage(G,imgOutG,smin,smax);

	cout << "Seuils B max min : "; 
	scanf("%i %i",&smin,&smax);
	seuillage(B,imgOutB,smin,smax);

	expand(imgOutR,imgOutR);
	expand(imgOutG,imgOutG);
	expand(imgOutB,imgOutB);

	for(int i = 0; i < imgOutR->getSize();i++)
	{
		imgOut->set(i,0,imgOutR->get(i,0));
		imgOut->set(i,1,imgOutG->get(i,1));
		imgOut->set(i,2,imgOutB->get(i,2));
	}
}

void mainEgalisation(ImageBase* image, ImageBase* imgOut)
{
	image->saveDDP(image->DDP(0),"ddp_base.dat");
	image->saveDDP(image->repartition(0),"repartition_base.dat");

	egalize(image,imgOut);
}

void mainSpecification(ImageBase* image, ImageBase* imgOut)
{
	egalize(image,imgOut);

	char referencePath[256];
	scanf("%s",&referencePath);
	ImageBase* reference = new ImageBase(); reference->load(referencePath);

	specify(imgOut,reference,imgOut);
}

int main(int argc, char **argv)
{
	if (argc != 4) 
	{
		printf("Usage: ImageIn ; ImageOut ;  Question \n   > 0 : erosion\n"); 
		return 1;
	}
	char imageInPath[250] , imageOutPath[250];int question;
	sscanf (argv[1],"%s",imageInPath) ;
	sscanf (argv[2],"%s",imageOutPath) ;
	sscanf (argv[3],"%d",&question) ;
	cout << endl;

	cout << "Loading image " << imageInPath << endl;

	ImageBase* image = new ImageBase(); image->load(imageInPath);
	cout << " > Size : " << image->getSize() << " | " << (image->getColor() ? "PPM" : "PGM") << endl;
	
	cout << "Saving histogram" << endl;

	if(!image->getColor())
	{
		image->saveHistogram(image->histogram(0),"histo_base.dat");
	}
	else
	{
		image->saveHistogram(image->histogram(0),"histo_baseR.dat");
		image->saveHistogram(image->histogram(1),"histo_baseG.dat");
		image->saveHistogram(image->histogram(2),"histo_baseB.dat");
	}

	ImageBase* imgOut = new ImageBase(image->getWidth(), image->getHeight(),image->getColor());

	switch(question)
	{
		default: return 0;
		case 0: mainExpansion(image,imgOut); break;
		case 1: mainSeuillage(image,imgOut); break;
		case 2: mainEgalisation(image,imgOut); break;
		case 3: mainSpecification(image,imgOut); break;
	}

	cout << "Saving" << endl;
	
	imgOut->save(imageOutPath);

	if(!imgOut->getColor())
	{
		imgOut->saveHistogram(imgOut->histogram(0),"histo_final.dat");
	}
	else
	{
		imgOut->saveHistogram(imgOut->histogram(0),"histo_finalR.dat");
		imgOut->saveHistogram(imgOut->histogram(1),"histo_finalG.dat");
		imgOut->saveHistogram(imgOut->histogram(2),"histo_finalB.dat");
	}
	
	return 0;
}

/*
///////////////////////////////////////// Exemple d'un seuillage d'image
	char cNomImgLue[250], cNomImgEcrite[250];
	int S;
  
	if (argc != 4) 
	{
		printf("Usage: ImageIn.pgm ImageOut.pgm Seuil \n"); 
		return 1;
	}
	sscanf (argv[1],"%s",cNomImgLue) ;
	sscanf (argv[2],"%s",cNomImgEcrite);
	sscanf (argv[3],"%d",&S);
	
	
	//ImageBase imIn, imOut;
	ImageBase imIn;
	imIn.load(cNomImgLue);

	//ImageBase imG(imIn.getWidth(), imIn.getHeight(), imIn.getColor());
	ImageBase imOut(imIn.getWidth(), imIn.getHeight(), imIn.getColor());

	for(int x = 0; x < imIn.getHeight(); ++x)
		for(int y = 0; y < imIn.getWidth(); ++y)
		{
			if (imIn[x][y] < S) 
				imOut[x][y] = 0;
			else imOut[x][y] = 255;
		}
		
	imOut.save(cNomImgEcrite);
		

	
	
	///////////////////////////////////////// Exemple de cr�ation d'une image couleur
	ImageBase imC(50, 100, true);

	for(int y = 0; y < imC.getHeight(); ++y)
		for(int x = 0; x < imC.getWidth(); ++x)
		{
			imC[y*3][x*3+0] = 200; // R
			imC[y*3][x*3+1] = 0; // G
			imC[y*3][x*3+2] = 0; // B
		}
		
	imC.save("imC.ppm");
		



	///////////////////////////////////////// Exemple de cr�ation d'une image en niveau de gris
	ImageBase imG(50, 100, false);

	for(int y = 0; y < imG.getHeight(); ++y)
		for(int x = 0; x < imG.getWidth(); ++x)
			imG[y][x] = 50;

	imG.save("imG.pgm");




	ImageBase imC2, imG2;
	
	///////////////////////////////////////// Exemple lecture image couleur
	imC2.load("imC.ppm");
	///////////////////////////////////////// Exemple lecture image en niveau de gris
	imG2.load("imG.pgm");
	
	

	///////////////////////////////////////// Exemple de r�cup�ration d'un plan de l'image
	ImageBase *R = imC2.getPlan(ImageBase::PLAN_R);
	R->save("R.pgm");
	delete R;
	*/
